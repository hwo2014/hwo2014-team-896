using System;
using Newtonsoft.Json;

namespace Bot {
	public class Track {
		public class Piece {
			public class Lane {
				public double distanceFromCenter;
				public double distanceFromStartToLapEnd;
			}

			[JsonProperty] public double length;
			[JsonProperty] public double radius;
			[JsonProperty] public double angle;
			[JsonProperty("switch")] public bool isSwitch;

            public int pieceIndex;

			public bool isBend { get { return length == 0; } }
			public Vector2 bendArcCenter;

			public Vector2 startPoint;
			public double startAngle;

			public Vector2 endPoint;
			public double endAngle;

			public Lane[] lanes;

			public SwitchData[][] switchData = null;

			public BendSwitchData GetBendSwitchData(int startLane, int endLane) {
				return (BendSwitchData)switchData[startLane][endLane];
			}

			public StraightSwitchData GetStraightSwitchData(int startLane, int endLane) {
				return (StraightSwitchData)switchData[startLane][endLane];
			}

            public double GetLaneBendRadius(double laneOffset) {
                return radius + (angle < 0 ? laneOffset : -laneOffset);
            }

            public double GetLaneSwitchBendRadius(double startOffset, double endOffset) {
                return 1.0 * Math.Min(GetLaneBendRadius(startOffset), GetLaneBendRadius(endOffset));
            }

            public double GetLaneSwitchBendRadius(int startLane, int endLane, double inPieceDistance) {
                BendSwitchData data = (BendSwitchData)switchData[startLane][endLane];
                if (data.switchRadiisKnown) {
                    return data.GetSwitchRadii(inPieceDistance);
                }
                else {
                    double startLaneOffset = lanes[startLane].distanceFromCenter;
                    double endLaneOffset = lanes[endLane].distanceFromCenter;
                    return 0.97 * Math.Min(GetLaneBendRadius(startLaneOffset), GetLaneBendRadius(endLaneOffset));
                }
            }

			public Vector2 EvalutePosition(double t, double laneOffset) {
				if (isBend) {
					return bendArcCenter + ((startPoint - bendArcCenter).Rotated(angle * t)).Normalized() * GetLaneBendRadius(laneOffset);
				} else {
					return startPoint + (endPoint - startPoint) * t + Vector2.FromAngle(startAngle).NormalPositive() * laneOffset;
				}
			}

            public double EvaluateAngle(double t) {
                return MathUtils.Lerp(startAngle, endAngle, t);
            }

            public double GetLaneLength(double laneOffset) {
                if (isBend) {
                    return GetLaneBendRadius(laneOffset) * Math.PI * (Math.Abs(angle) / 180.0);
                }
                else {
                    return length;
                }
            }

			public double GetLaneSwitchLength(int startLane, int endLane) {
				SwitchData data = switchData[startLane][endLane];
				if (data.switchLengthKnown) return data.switchLength;

                if (isBend) {
					double startLaneOffset = lanes[startLane].distanceFromCenter;
					double endLaneOffset = lanes[endLane].distanceFromCenter;

                    double midRadius = GetLaneBendRadius((startLaneOffset + endLaneOffset) / 2.0);
                    double midLength = GetLaneLength((startLaneOffset + endLaneOffset) / 2.0);

                    double A = 1.343217474, B = -0.00123272, C = 0.88576095, D = -0.000922961;

                    return midLength * Math.Max(1.0, Math.Min(1.1, (A + B * midRadius) * (C + D * Math.Abs(angle))));
                }
                else {
                    return length * 1.0125;
                }
			}

            public double GetPieceLength(int startLane, int endLane) {
                if (startLane != endLane) return GetLaneSwitchLength(startLane, endLane);
                return GetLaneLength(lanes[startLane].distanceFromCenter);
            }

            public override string ToString() {
                return string.Format("pieceIndex: {0}, Length: {1}, radius: {2}, angle: {3}, isSwitch: {4}", pieceIndex, length, radius, angle, isSwitch);
            }
		}

		public class Lane {
			[JsonProperty] public double distanceFromCenter;
			[JsonProperty] public int index;
		}

		public class StartingPoint {
			public class Point {
				[JsonProperty] public double x;
				[JsonProperty] public double y;
			}

			[JsonProperty] public Point position;
			[JsonProperty] public double angle;
		}

		[JsonProperty] public string id;
		[JsonProperty] public string name;

		[JsonProperty] public Piece[] pieces;
		[JsonProperty] public Lane[] lanes;
		[JsonProperty] public StartingPoint startingPoint;

		public double lapLength;
		public Vector2 boundsMin;
		public Vector2 boundsMax;

		public double shortestLapLength;

		public void BuildTrack(DataHandler dataHandler) {
			Vector2 currentPoint = new Vector2(startingPoint.position.x, startingPoint.position.y);
			double currentAngle = startingPoint.angle;
			//double currentDistance = 0.0;

			for (int pieceIndex=0; pieceIndex < pieces.Length; ++pieceIndex) {
				Piece piece = pieces[pieceIndex];

				piece.lanes = new Piece.Lane[lanes.Length];
				for (int j=0; j<lanes.Length; ++j) piece.lanes[j] = new Piece.Lane() { distanceFromCenter = lanes[j].distanceFromCenter };

                piece.pieceIndex = pieceIndex;

				piece.startPoint = currentPoint;
				piece.startAngle = currentAngle;

				Vector2 direction = Vector2.FromAngle(currentAngle);

				if (piece.isBend) {
					piece.bendArcCenter = piece.startPoint + (piece.angle < 0.0f ? direction.NormalNegative() : direction.NormalPositive()) * piece.radius;
					//piece.pieceLength = piece.radius * Math.PI * (Math.Abs(piece.angle) / 180.0);

					currentPoint = piece.bendArcCenter + (piece.startPoint - piece.bendArcCenter).Rotated(piece.angle);
					currentAngle += piece.angle;
				} else {
					//piece.pieceLength = piece.length;

					currentPoint += direction * piece.length;
				}

				//currentDistance += piece.pieceLength;
				piece.endPoint = currentPoint;
				piece.endAngle = currentAngle;
			}

			{
				for (int lane = 0; lane < lanes.Length; ++lane) {
					Piece piece = pieces[pieces.Length - 1];
					piece.lanes[lane].distanceFromStartToLapEnd = piece.GetLaneLength(lanes[lane].distanceFromCenter);
				}

				for (int i=pieces.Length - 2; i >= 0; --i) {
					Piece piece = pieces[i];
					Piece nextPiece = pieces[i + 1];

					for (int lane = 0; lane < lanes.Length; ++lane) {
						double minDistance = double.MaxValue;

						for (int nextLane = lane - 1; nextLane <= lane + 1; ++nextLane) {
							if (!piece.isSwitch && nextLane != lane) continue;
							if (nextLane < 0 || nextLane >= lanes.Length) continue;

							double pieceStartLaneLength = piece.GetLaneLength(lanes[lane].distanceFromCenter);
							double pieceEndLaneLength = piece.GetLaneLength(lanes[nextLane].distanceFromCenter);

							double pieceLength = (pieceStartLaneLength + pieceEndLaneLength) / 2.0;

							if (lane != nextLane) pieceLength *= 1.03;

							double distance = pieceLength + nextPiece.lanes[nextLane].distanceFromStartToLapEnd;

							if (distance < minDistance) minDistance = distance;
						}

						piece.lanes[lane].distanceFromStartToLapEnd = minDistance;
					}
				}

				shortestLapLength = double.MaxValue;
				for (int lane = 0; lane < lanes.Length; ++lane) {
					Piece piece = pieces[0];
					if (piece.lanes[lane].distanceFromStartToLapEnd < shortestLapLength) shortestLapLength = piece.lanes[lane].distanceFromStartToLapEnd;
				}
			}

			foreach (Piece piece in pieces) {
				if (piece.isSwitch) {
					piece.switchData = new SwitchData[lanes.Length][];
					for (int i=0; i<lanes.Length; ++i) {
						piece.switchData[i] = new SwitchData[lanes.Length];
					}

					for (int startLane = 0; startLane < lanes.Length; ++startLane) {
						for (int endLane = Math.Max(0, startLane - 1); endLane <= Math.Min(lanes.Length - 1, startLane + 1); ++endLane) {
							if (startLane == endLane) continue;

							if (piece.isBend) {
								double r0 = piece.GetLaneBendRadius(lanes[startLane].distanceFromCenter);
								double r1 = piece.GetLaneBendRadius(lanes[endLane].distanceFromCenter);
								piece.switchData[startLane][endLane] = dataHandler.GetBendSwitchData(piece.angle, r0, r1, true);


                                /*BendSwitchCalculator calc = new BendSwitchCalculator() { angle = piece.angle, r0 = r0, r1 = r1 };
                                calc.Process();

                                Console.WriteLine("{0}_{1}_{2}: {3} {4} {5}", (int)Math.Abs(piece.angle), (int)r0, (int)r1, calc.switchLength, piece.GetLaneLength(lanes[startLane].distanceFromCenter), piece.GetLaneLength(lanes[endLane].distanceFromCenter));

                                if ((int)Math.Abs(piece.angle) == 45 && (int)r0 == 100 && (int)r1 == 80) {
                                    for (int i = 0; i < calc.switchRadiis.Length; ++i) {
                                        Console.WriteLine("{0}", calc.switchRadiis[i]);
                                    }
                                }*/

							} else {
								double width = Math.Abs(lanes[startLane].distanceFromCenter - lanes[endLane].distanceFromCenter);
								piece.switchData[startLane][endLane] = dataHandler.GetStraightSwitchData(piece.length, width, true);
							}
						}
					}
				}
			}

			//lapLength = currentDistance;

			boundsMin = new Vector2(double.MaxValue, double.MaxValue);
			boundsMax = new Vector2(double.MinValue, double.MinValue);

			foreach (Piece piece in pieces) {
				boundsMin = Vector2.Min(boundsMin, piece.startPoint);
				boundsMin = Vector2.Min(boundsMin, piece.endPoint);

				boundsMax = Vector2.Max(boundsMax, piece.startPoint);
				boundsMax = Vector2.Max(boundsMax, piece.endPoint);
			}
		}

        public Piece GetNextPiece(Piece piece) {
            int nextIndex = piece.pieceIndex + 1;
            if (nextIndex >= pieces.Length) nextIndex = 0;
            return pieces[nextIndex];
        }

		public override string ToString() {
			return JsonConvert.SerializeObject(this);
		}
	}

	public class Race {
		public class Car {
			public class Dimensions {
				[JsonProperty] public double length;
				[JsonProperty] public double width;
				[JsonProperty] public double guideFlagPosition;
			}

			[JsonProperty] public CarId id;
			[JsonProperty] public Dimensions dimensions;
		}

		[JsonProperty] public Track track;
		[JsonProperty] public Car[] cars;
	}

	public class CarId {
		public string name;
		public string color;

        public override string ToString() {
            return string.Format("{0} ({1})", name, color);
        }
	}

	public class PiecePosition {
		public class Lane {
			public int startLaneIndex;
			public int endLaneIndex;
		}

		public int pieceIndex;
		public double inPieceDistance;
		public Lane lane;
		public int lap;

		//public double GetTotalDistance(Track track) {
		//	return lap * track.lapLength + track.pieces[pieceIndex].startDistance + inPieceDistance;
		//}

        public double GetPieceLength(Track track) {
            if (lane.startLaneIndex != lane.endLaneIndex) {
                return track.pieces[pieceIndex].GetLaneSwitchLength(lane.startLaneIndex, lane.endLaneIndex);
            }

            return track.pieces[pieceIndex].GetLaneLength(track.lanes[lane.endLaneIndex].distanceFromCenter);
        }

        public double DistanceToPieceEnd(Track track) {
            return GetPieceLength(track) - inPieceDistance;
        }

        public Vector2 GetTrackPoint(Track track) {
            double t = inPieceDistance / GetPieceLength(track);
            return track.pieces[pieceIndex].EvalutePosition(t, MathUtils.Lerp(track.lanes[lane.startLaneIndex].distanceFromCenter, track.lanes[lane.endLaneIndex].distanceFromCenter, t));
        }

        public double GetTrackAngle(Track track) {
            double t = inPieceDistance / GetPieceLength(track);
            return track.pieces[pieceIndex].EvaluateAngle(t);
        }

        public double GetBendRadius(Track track) {
            if (lane.startLaneIndex != lane.endLaneIndex) {
                return track.pieces[pieceIndex].GetLaneSwitchBendRadius(track.lanes[lane.startLaneIndex].distanceFromCenter, track.lanes[lane.endLaneIndex].distanceFromCenter);
            }

            return GetStartLaneBendRadius(track);
        }

        public double GetStartLaneBendRadius(Track track) {
            return track.pieces[pieceIndex].GetLaneBendRadius(track.lanes[lane.startLaneIndex].distanceFromCenter);
        }

        public double GetEndLaneBendRadius(Track track) {
			return track.pieces[pieceIndex].GetLaneBendRadius(track.lanes[lane.endLaneIndex].distanceFromCenter);
		}

		public double GetDistanceToEndOfLap(Track track) {
			return track.pieces[pieceIndex].lanes[lane.endLaneIndex].distanceFromStartToLapEnd - inPieceDistance;
		}

		public bool IsInFrontOfPosition(Track track, PiecePosition other) {
			if (pieceIndex == other.pieceIndex) {
				return inPieceDistance > other.inPieceDistance;
			} else {
				int pieceDiff = pieceIndex - other.pieceIndex;
				if (pieceDiff < 0) pieceDiff += track.pieces.Length;
				return pieceDiff < track.pieces.Length / 2;
			}
		}

        public bool IsSwitchingLane() {
            return lane.startLaneIndex != lane.endLaneIndex;
        }

        public string ToString(Track track) {
            return string.Format("piece: {0}, inPieceDistance: {1}, start lane: {2} ({3}), end lane: {4} ({5}), lap: {6}",
                pieceIndex, inPieceDistance, 
                lane.startLaneIndex, track.lanes[lane.startLaneIndex].distanceFromCenter,
                lane.endLaneIndex, track.lanes[lane.endLaneIndex].distanceFromCenter,
                lap);
        }
	}

	public class CarPosition {
		public CarId id;
		public double angle;
		public PiecePosition piecePosition;

        public double GetAbsoluteAngle(Track track) {
            return piecePosition.GetTrackAngle(track) + angle;
        }
	}

    public class LapFinished {
        public class LapTime {
            public int lap;
            public int ticks;
            public int millis;
        }

        public class RaceTime {
            public int laps;
            public int ticks;
            public int millis;
        }

        public class Ranking {
            public int overall;
            public int fastestLap;
        }

        public CarId car;
        public LapTime lapTime;
        public RaceTime raceTime;
        public Ranking ranking;
    }

    public class DidNotFinish {
        public CarId car;
        public string reason;
    }

    public class TurboAvailable {
        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;
    }

	public class Wrapper {
		[JsonProperty] public string msgType;
		[JsonProperty] public Object data;
		[JsonProperty] public string gameId = null;
		[JsonProperty("gameTick")] public string gameTickStr = null;

		[JsonIgnore] public int gameTick { get { return gameTickStr != null ? int.Parse(gameTickStr) : 0; } }

		public Newtonsoft.Json.Linq.JObject jdata { get { return (Newtonsoft.Json.Linq.JObject)data; } }
		public Newtonsoft.Json.Linq.JArray jarray { get { return (Newtonsoft.Json.Linq.JArray)data; } }

		public Wrapper(string msgType, Object data) {
			this.msgType = msgType;
			this.data = data;
		}
	}
}

