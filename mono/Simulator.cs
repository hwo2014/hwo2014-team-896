﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot {

    public class CarState {
        public CarState prevState;
		public int gameTick;

        public Track.Piece piece;
        public double pieceLength;
        public double inPieceDistance;
		public int lap;

        public double speed;
        public double throttle;

        public double angle;
        public double angularSpeed;

        public double startLaneOffset;
        public double endLaneOffset;
        public bool canSwitchLane;
        public int startLaneIndex;
        public int endLaneIndex;

        public bool crashed;
        public int ticksUntilRespawn;
		public bool bumpedCar;

        public PiecePosition piecePosition {
            get {
                return new PiecePosition() {
                    pieceIndex = piece.pieceIndex,
                    inPieceDistance = inPieceDistance,
                    lane = new PiecePosition.Lane() {
                        startLaneIndex = startLaneIndex,
                        endLaneIndex = endLaneIndex
                    }, 
                    lap = lap
                };
            }
        }

        public CarState Clone() {
            CarState clone = new CarState() {
                prevState = prevState,
                piece = piece,
                pieceLength = pieceLength, 
                inPieceDistance = inPieceDistance, 
				lap = lap,
                speed = speed, 
                throttle = throttle,
                angle = angle, 
                angularSpeed = angularSpeed, 
                startLaneOffset = startLaneOffset, 
                endLaneOffset = endLaneOffset,
                startLaneIndex = startLaneIndex, 
                endLaneIndex = endLaneIndex, 
                crashed = crashed, 
                ticksUntilRespawn = ticksUntilRespawn,
				bumpedCar = bumpedCar
            };
            return clone;
        }

        public CarState[] ToArray() {
            List<CarState> states = new List<CarState>();
            for (CarState state = this; state != null; state = state.prevState) {
                states.Add(state);
            }
            states.Reverse();
            return states.ToArray();
        }

        public bool CrashedBeforeBend() {
            if (!crashed) return false;
            CarState[] states = ToArray();

            foreach (CarState state in states) {
                if (state.piece.isBend) return false;
                if (state.crashed) return true;
            }

            return false;
        }
    }

    public class TrackRoute {
        public class Piece {
            public int startLaneIndex;
            public int endLaneIndex;
        }

        public Piece[] pieces;

        public TrackRoute(int numPieces) {
            pieces = new TrackRoute.Piece[numPieces];
            for (int i = 0; i < pieces.Length; ++i) {
                pieces[i] = new Piece() { startLaneIndex = -1, endLaneIndex = -1 };
            }
        }
    }

	public class TrackState {

		public class Car {
			public int pieceIndex;
			public int laneIndex;
			public double inPieceDistance;
			public double speed;
		}

		public class Piece {
			public class Lane {
				public List<Car> cars;
			}

			public Lane[] lanes;
		}

		public Track track;
		public Piece[] pieces;

		Piece GetPieceState(int pieceIndex, bool createIfNecessary = false) { 
			if (pieces == null) {
				if (createIfNecessary) pieces = new Piece[track.pieces.Length];
				else return null;
			}

			Piece pieceState = pieces[pieceIndex];
			if (pieceState == null && createIfNecessary) {
				pieceState = new Piece();
				pieceState.lanes = new Piece.Lane[track.lanes.Length];
				for (int i=0; i<pieceState.lanes.Length; ++i) pieceState.lanes[i] = new Piece.Lane();
				pieces[pieceIndex] = pieceState;
			}

			return pieceState;
		}

		public List<Car> GetCars(int pieceIndex, int laneIndex) {
			Piece pieceState = GetPieceState(pieceIndex);
			return pieceState != null ? pieceState.lanes[laneIndex].cars : null;
		}

		public void AddCar(Car car) {
			Piece pieceState = GetPieceState(car.pieceIndex, true);
			List<Car> cars = pieceState.lanes[car.laneIndex].cars;
			if (cars == null) {
				cars = new List<Car>();
				pieceState.lanes[car.laneIndex].cars = cars;
			}
			cars.Add(car);
		}
	}

    public class Simulator {


		public ILogger logger;
        public Track track;
        public PhysicsParams physicsParams;
        public double turboFactor = 1.0;
		public double carLength = 40.0;

		public int startGameTick = 0;

		//Random random = new Random();

		List<TrackState> trackStates = new List<TrackState>();

		public void Init() {
		}

		public void AddOtherCar(CarState state, CarStats stats, int simulationTicks) {
			CarState newState = state.Clone();

            double prevKnownSpeed = newState.speed;

			for (int tick=0; tick<simulationTicks; ++tick) {
				if (tick >= trackStates.Count) {
					trackStates.Add(new TrackState() { track = track });
				}

                if (newState.crashed) {
                    newState.ticksUntilRespawn--;
                    if (newState.ticksUntilRespawn <= 0) newState.crashed = false;
                    else continue;
                }

				TrackState trackState = trackStates[tick];

				trackState.AddCar(new TrackState.Car() {
					pieceIndex = newState.piece.pieceIndex,
					laneIndex = newState.endLaneIndex,
					inPieceDistance = newState.inPieceDistance,
					speed = newState.speed
				});

				newState = SimpleTick(newState);

                double newSpeed;

                if (stats.TryGetSpeed(newState.piecePosition, out prevKnownSpeed)) {
                    newSpeed = prevKnownSpeed;
                }
                else {
                    newSpeed = prevKnownSpeed * 0.8;
                }

                double throttle = physicsParams.GetThrottleForTargetSpeed(newState.speed, newSpeed, 1.0);
                newState.speed = state.speed * (1.0 - physicsParams.drag) + throttle * physicsParams.power;

				trackState.AddCar(new TrackState.Car() {
					pieceIndex = newState.piece.pieceIndex,
					laneIndex = newState.endLaneIndex,
					inPieceDistance = newState.inPieceDistance,
					speed = newState.speed
				});
			}
		}

		public CarState SimpleTick(CarState state) {
			CarState newState = state.Clone();
			newState.prevState = state;
			newState.gameTick = state.gameTick + 1;

			newState.inPieceDistance += newState.speed;

			if (newState.inPieceDistance >= newState.pieceLength) {
				newState.inPieceDistance -= newState.pieceLength;
				newState.piece = track.GetNextPiece(newState.piece);
				if (newState.piece.pieceIndex == 0) newState.lap++;

                newState.startLaneIndex = newState.endLaneIndex;
                newState.startLaneOffset = newState.endLaneOffset;

				newState.pieceLength = newState.piece.GetLaneLength(newState.endLaneOffset);
			}

			return newState;
		}

        public bool WillBumpOtherCar(CarState state, CarStats stats, TrackRoute route, Track.Piece beforePiece, out int bumpTick) {
            bumpTick = -1;

            CarState newState = state;
            while (true) {

                int lane = newState.endLaneIndex;
                if (newState.canSwitchLane && route.pieces[newState.piece.pieceIndex].endLaneIndex >= 0) {
                    lane = route.pieces[newState.piece.pieceIndex].endLaneIndex;
                }

                double targetSpeed;
                if (!stats.TryGetSpeed(newState.piecePosition, out targetSpeed)) {
                    targetSpeed = newState.speed;
                }

                double throttle = physicsParams.GetThrottleForTargetSpeed(newState.speed, targetSpeed, turboFactor);

                newState = Tick(newState, throttle, lane);
                if (newState.piece == beforePiece) return false;

                TrackState trackState = GetTrackState(newState.gameTick);
                if (trackState == null) return false;

                if (newState.bumpedCar) {
                    bumpTick = newState.gameTick;
                    return true;
                }
            }
        }

        TrackState GetTrackState(int gameTick) {
			if (trackStates == null || trackStates.Count == 0) return null;
			//int tick = Math.Max(0, Math.Min(trackStates.Count - 1, gameTick - startGameTick));
			int tick = gameTick - startGameTick;
			if (tick < 0 || tick >= trackStates.Count) return null;
			return trackStates[tick];
		}

        public CarState Tick(CarState state, double throttle, int endLaneIndex) {
            bool slipped;
            return Tick(state, throttle, endLaneIndex, out slipped);
        }

        public CarState Tick(CarState state, double throttle, int endLaneIndex, out bool slipped) {
            slipped = false;

            CarState newState = state.Clone();
            newState.prevState = state;
			newState.gameTick = state.gameTick + 1;
            newState.canSwitchLane = false;

            if (endLaneIndex != newState.endLaneIndex) {
                if (endLaneIndex != newState.startLaneIndex) {
                    newState.endLaneOffset = track.lanes[endLaneIndex].distanceFromCenter;
                    newState.pieceLength = newState.piece.GetLaneSwitchLength(newState.startLaneIndex, endLaneIndex);
                }
                newState.endLaneIndex = endLaneIndex;
            }

            newState.angularSpeed = state.angularSpeed * (1.0 - physicsParams.slipDampeningFactor) - state.angle * state.speed * physicsParams.slipAngleAccelerationFactor;

            if (state.piece.isBend) {

                double radius = newState.startLaneIndex != newState.endLaneIndex ? 
                    state.piece.GetLaneSwitchBendRadius(newState.startLaneIndex, newState.endLaneIndex, newState.inPieceDistance) - 2.0 : 
                    state.piece.GetLaneBendRadius(newState.startLaneOffset);

                double centrifugalForce = physicsParams.slipCentrifugalForceFactor * state.speed * state.speed / Math.Sqrt(radius);
                double downforce = physicsParams.slipDownforceFactor * physicsParams.slipAngleAccelerationFactor * state.speed;

                double force = centrifugalForce - downforce;

                if (force > 0.0) {
                    slipped = true;

                    if (state.piece.angle < 0) {
                        newState.angularSpeed -= force;
                    }
                    else {
                        newState.angularSpeed += force;
                    }
                }
            }

            newState.angle += newState.angularSpeed;

            if (newState.angle < -physicsParams.maxSlipAngle || newState.angle > physicsParams.maxSlipAngle) {
                newState.crashed = true;
            }

            newState.throttle = throttle;
            newState.speed = state.speed * (1.0 - physicsParams.drag) + throttle * turboFactor * physicsParams.power;
            newState.inPieceDistance += newState.speed;

			TrackState trackState = state.gameTick - startGameTick >= 4 ? GetTrackState(state.gameTick) : null;

			if (trackState != null && !state.bumpedCar) {
				List<TrackState.Car> cars = trackState.GetCars(state.piece.pieceIndex, state.endLaneIndex);
				if (cars != null) {
					foreach (TrackState.Car car in cars) {
						if (Math.Abs(newState.inPieceDistance - car.inPieceDistance) < carLength) {
							//newState.speed *= 0.6;
							newState.bumpedCar = true;
						}
					}
				}
			}

            if (newState.inPieceDistance >= newState.pieceLength) {
                newState.inPieceDistance -= newState.pieceLength;
                newState.piece = track.GetNextPiece(newState.piece);
				if (newState.piece.pieceIndex == 0) newState.lap++;
                newState.canSwitchLane = newState.piece.isSwitch;// && newState.speed < 9;

                newState.startLaneIndex = state.endLaneIndex;
                newState.startLaneOffset = state.endLaneOffset;

                newState.pieceLength = newState.piece.GetLaneLength(newState.startLaneOffset);


				if (trackState != null && !state.bumpedCar && !newState.bumpedCar) {
					List<TrackState.Car> cars = trackState.GetCars(newState.piece.pieceIndex, newState.endLaneIndex);
					if (cars != null) {
						foreach (TrackState.Car car in cars) {
							if (Math.Abs(newState.inPieceDistance - car.inPieceDistance) < carLength) {
								//newState.speed *= 0.6;
								newState.bumpedCar = true;
							}
						}
					}
				}
            }

            return newState;
        }

		public double DistanceFromStateToEndOfLap(CarState state, int lap) {
			double d = state.piece.lanes[state.startLaneIndex].distanceFromStartToLapEnd - state.inPieceDistance;
			if (state.lap < lap) d += (lap - state.lap) * track.shortestLapLength;
			return d;
		}

		public int CompareStates(CarState s1, CarState s2) {
			if (s2 == null) return -1;
			if (s1 == null) return 1;
			if (s2.crashed) return -1;
			if (s1.crashed) return 1;

			int maxLap = Math.Max(s1.lap, s2.lap);

			double d1 = DistanceFromStateToEndOfLap(s1, maxLap);
			double d2 = DistanceFromStateToEndOfLap(s2, maxLap);

			return d1 < d2 ? -1 : 1;
		}

		public CarState ChooseBestState(CarState s1, CarState s2) {
			int comparisonResult = CompareStates(s1, s2);
			return comparisonResult < 0 ? s1 : s2;
		}

		public CarState GetMaxThrottle(CarState state, TrackRoute route, int throttleSteps, int throttleTicks, int maxTick) {
			for (int step = 0; step < throttleSteps; ++step) {
				double throttle = MathUtils.Lerp(1, 0, step / (double)(throttleSteps - 1));

				CarState newState = state;
				while (!newState.crashed && newState.gameTick < maxTick) {
                    int lane = newState.endLaneIndex;
                    if (newState.canSwitchLane && route.pieces[newState.piece.pieceIndex].endLaneIndex >= 0) {
                        lane = route.pieces[newState.piece.pieceIndex].endLaneIndex;
                    }

					newState = Tick(newState, newState.gameTick < state.gameTick + throttleTicks ? throttle : 0.0, lane);
				}

				if (!newState.crashed) {
                    return newState;
				}
			}

            return null;
		}

        public bool WillCrash(CarState state, TrackRoute route, int maxTick) {
            CarState newState = state;
            while (!newState.crashed && newState.gameTick < maxTick) {
                int lane = newState.endLaneIndex;
                if (route != null && newState.canSwitchLane && route.pieces[newState.piece.pieceIndex].endLaneIndex >= 0) {
                    lane = route.pieces[newState.piece.pieceIndex].endLaneIndex;
                }

                newState = Tick(newState, 0.0, lane);
            }

            return newState.crashed;
        }

        public CarState OptimalThrottleHelper(CarState state, TrackRoute route, int throttleSteps, int throttleTicks, int maxTick, 
            Track.Piece firstBendStartPiece, double firstBendMaxConstantDistance, out double maxAngleReachedInFirstBend, out bool slippedOnEachTickInFirstBend, out double initialThrottle) {

            CarState result = null;

            maxAngleReachedInFirstBend = 0.0;
            slippedOnEachTickInFirstBend = true;
            initialThrottle = 1.0;

            for (int step = 0; step < throttleSteps; ++step) {
                initialThrottle = MathUtils.Lerp(1, 0, step / (double)(throttleSteps - 1));

                maxAngleReachedInFirstBend = 0.0;
                slippedOnEachTickInFirstBend = true;

                bool firstBendReached = false;
                bool currentlyInFirstBend = false;
                double distanceTraveledInFirstPiece = 0.0;


                CarState newState = state;
                while (!newState.crashed && newState.gameTick < maxTick) {
                    int lane = newState.endLaneIndex;
                    if (route != null && newState.canSwitchLane && route.pieces[newState.piece.pieceIndex].endLaneIndex >= 0) {
                        lane = route.pieces[newState.piece.pieceIndex].endLaneIndex;
                    }

                    bool useConstantSpeedInBend = true;
                    if (!firstBendReached) {
                        if (newState.piece == firstBendStartPiece) {
                            firstBendReached = true;
                            currentlyInFirstBend = true;
                        }
                    }

                    if (currentlyInFirstBend) {
                        if (!newState.piece.isBend || newState.piece.angle * firstBendStartPiece.angle < 0) {
                            currentlyInFirstBend = false;
                        }
                        else if (distanceTraveledInFirstPiece >= firstBendMaxConstantDistance) {
                            useConstantSpeedInBend = false;
                        }
                    }

                    if (!currentlyInFirstBend) {
                        useConstantSpeedInBend = false;
                    }

                    double throttle = 0.0;
                    if (newState.gameTick < state.gameTick + throttleTicks) {
                        throttle = initialThrottle;
                    }
                    else if (newState.piece.isBend && useConstantSpeedInBend) {
                        throttle = physicsParams.GetThrottleForTargetSpeed(newState.speed, newState.speed, turboFactor);
                    }

                    bool slipped;
                    newState = Tick(newState, throttle, lane, out slipped);
                    if (currentlyInFirstBend && !slipped) slippedOnEachTickInFirstBend = false;

                    if (currentlyInFirstBend) {
                        distanceTraveledInFirstPiece += newState.speed;

                        double angle = firstBendStartPiece.angle > 0 ? newState.angle : -newState.angle;
                        maxAngleReachedInFirstBend = Math.Max(maxAngleReachedInFirstBend, angle);
                    }
                }

                result = newState;
                if (!result.crashed) break;
            }

            return result;
        }

        public CarState GetOptimalThrottleV1(CarState state, TrackRoute route, int throttleSteps, int throttleTicks, int maxTick) {
            double maxAngle, throttle;
            bool slippedOnEachTick;
            return OptimalThrottleHelper(state, route, throttleSteps, throttleTicks, maxTick, null, 0, out maxAngle, out slippedOnEachTick, out throttle);
        }

        public CarState GetOptimalThrottleV2(CarState state, TrackRoute route, int throttleSteps, int throttleTicks, int maxTick) {

            Track.Piece firstBendStart, firstBendEnd;
            GetFirstBendRange(state.piece, out firstBendStart, out firstBendEnd);

            double firstBendLength = GetDistanceBetweenPieces(firstBendStart, firstBendEnd, route);
            if (firstBendStart == state.piece) firstBendLength -= state.inPieceDistance;

            //logger.Log(firstBendStart.pieceIndex + " " + firstBendEnd.pieceIndex);
            //logger.Log(string.Format("Bend start: {0}, length: {1}", GetDistanceBetweenPieces(state.piece, firstBendStart, route), firstBendLength));

            CarState result = null;

            int steps = 10;
            for (int i = 0; i < steps; ++i) {
                double distanceFactor = MathUtils.Lerp(1.0, 0.0, i / (double)(steps - 1));

                double maxAngle, throttle;
                bool slippedOnEachTick;

                result = OptimalThrottleHelper(state, route, throttleSteps, throttleTicks, maxTick, firstBendStart, firstBendLength * distanceFactor, out maxAngle, out slippedOnEachTick, out throttle);

                if (!result.crashed) {
                    if (throttle >= 0.9999 || (slippedOnEachTick && maxAngle >= physicsParams.maxSlipAngle * 0.99)) {
                        logger.Log("Distance factor used: " + distanceFactor);
                        break;
                    }
                }
            }

            return result;
        }

        public bool CanDriveAtThrottleToPiece(CarState state, TrackRoute route, double throttle, Track.Piece targetPiece, int maxFullThrottleTick, int maxTick) {
            bool reachedPiece = false;

            if (state.piece == targetPiece) return true;

            CarState newState = state;
            while (newState.gameTick < maxTick) {
                int lane = newState.endLaneIndex;
                if (newState.canSwitchLane && route.pieces[newState.piece.pieceIndex].endLaneIndex >= 0) {
                    lane = route.pieces[newState.piece.pieceIndex].endLaneIndex;
                }

                if (newState.gameTick < maxFullThrottleTick) {
                    newState = Tick(newState, throttle, lane);
                    if (newState.piece == targetPiece) reachedPiece = true;
                }
                else {
                    newState = Tick(newState, 0.0, lane);
                }

                if (newState.crashed) return false;
            }

            return reachedPiece;
        }

        public CarState SuchSimulation(CarState state, int throttleDecisionsCount, int maxTick) {
			if (state.gameTick >= maxTick) return state;
			if (state.crashed) return state;

			CarState result = null;

			if (state.canSwitchLane) {
                int shortestLane = state.startLaneIndex;
                double shortestLaneLength = double.MaxValue;

				for (int laneIndex = state.startLaneIndex - 1; laneIndex <= state.startLaneIndex + 1; ++laneIndex) {
                    if (laneIndex < 0 || laneIndex >= track.lanes.Length) continue;
					/*CarState newState = SuchSimulation(Tick(state, state.throttle, laneIndex), throttleDecisionsCount, maxTick);
					if (newState != null && !newState.crashed) {
						if (result == null) {
							result = newState;
						} else {
							result = ChooseBestState(result, newState);
						}
					}*/

                    double distanceToPieceEnd;

                    if (laneIndex == state.startLaneIndex) {
                        distanceToPieceEnd = state.piece.GetLaneLength(state.startLaneOffset);
                    }
                    else {
                        distanceToPieceEnd = state.piece.GetLaneSwitchLength(state.startLaneIndex, laneIndex);
                    }

                    double length = distanceToPieceEnd + track.GetNextPiece(state.piece).lanes[laneIndex].distanceFromStartToLapEnd;

                    if (length < shortestLaneLength) {
                        shortestLaneLength = length;
                        shortestLane = laneIndex;
                    }
				}

                result = SuchSimulation(Tick(state, state.throttle, shortestLane), throttleDecisionsCount, maxTick);
			} else {

				//CarState maxThrottleEndState;
				//double maxThrottle = GetMaxThrottle(state, throttleDecisionsCount == 0 ? 21 : 11, 30, maxTick, out maxThrottleEndState);

                int throttleTicks;
				int throttleSteps;
                if (throttleDecisionsCount == 0) {
                    throttleSteps = 31;
                    throttleTicks = 10;
                }
                else if (throttleDecisionsCount == 1) {
                    throttleSteps = 16;
                    throttleTicks = 10;
                }
                else {
                    throttleSteps = 1;
                    throttleTicks = 100;
                }

				bool maxThrottleFound = false;
				//double maxThrottle = 0.0;

				for (int step = 0; step < throttleSteps; ++step) {
					double throttle = throttleSteps > 1 ? MathUtils.Lerp(1.0, 0, step / (double)(throttleSteps - 1)) : 0.0;

					if (maxThrottleFound) break;// && throttle < maxThrottle * 0.6) break;

					CarState newState = state;
					for (int i=0; i<throttleTicks; ++i) {
						newState = Tick(newState, throttle, newState.endLaneIndex);
						if (newState.crashed || newState.canSwitchLane || newState.gameTick >= maxTick) break;
					}

					newState = SuchSimulation(newState, throttleDecisionsCount + 1, maxTick);

					if (newState != null && !newState.crashed) {
						if (!maxThrottleFound) {
							maxThrottleFound = true;
							//maxThrottle = throttle;
						}

						if (result == null) {
							result = newState;
						} else {
							result = ChooseBestState(result, newState);
						}
					}
				}
			}

			return result;
		}

        public CarState SimulateSwitchDataGathering(CarState state, int depth, int maxDepth, int prevSwitchDirection) {
            if (depth >= maxDepth) return state;
            if (state.crashed) return state;

            CarState result = null;

            if (depth == 0) {
                int throttleSteps = 21;

                for (int step = 0; step < throttleSteps; ++step) {
                    double throttle = MathUtils.Lerp(1, 0, step / (double)(throttleSteps - 1));

                    CarState newState = Tick(state, throttle, state.endLaneIndex);

                    result = SimulateSwitchDataGathering(newState, depth + 1, maxDepth, prevSwitchDirection);
                    if (!result.crashed) {
                        break;
                    }
                }
            }
            else {
				int laneIndex = state.endLaneIndex;

				if (state.canSwitchLane) {
					if (laneIndex == 0) laneIndex++;
					else if (laneIndex == track.lanes.Length - 1) laneIndex--;
					else if (prevSwitchDirection > 0) laneIndex++;
					else laneIndex--;
				}

				CarState newState = Tick(state, depth < 3 ? state.throttle : 0.0, laneIndex);

				if (state.piece.isBend && state.piece.isSwitch && !newState.piece.isSwitch) {
					result = newState;
				} else {
					result = SimulateSwitchDataGathering(newState, depth + 1, maxDepth, prevSwitchDirection);
				}
            }

            return result;
        }

		public bool FindOptimalTurboStartPiece(int turboTicks, out Track.Piece turboStartPiece, out Track.Piece turboEndPiece) {
			turboStartPiece = null;
			turboEndPiece = null;

			double turboDistance = physicsParams.maxSpeed * turboTicks * 3 / 2;

			PhysicsParams.TrackParams trackParams = physicsParams.GetTrackParams(track);

			double bestThrottle = 0.0;

			foreach (Track.Piece startPiece in track.pieces) {
				if (startPiece.isBend) continue;

				double distance = 0.0;
				double throttle = 0.0;

				Track.Piece currentPiece = startPiece;
				while (true) {
					double pieceLength = currentPiece.GetLaneLength(0.0);
					distance += pieceLength;
					throttle += trackParams.pieces[currentPiece.pieceIndex].averageThrottle * pieceLength;

					if (distance >= turboDistance) break;

					currentPiece = track.GetNextPiece(currentPiece);
					if (currentPiece == startPiece) break;
				}

				if (throttle > bestThrottle) {
					bestThrottle = throttle;
					turboStartPiece = startPiece;
					turboEndPiece = track.GetNextPiece(currentPiece);
				}
			}

			return true;
		}

        public bool FindOptimalTurboStartPieceV2(Track.Piece searchStartPiece, int turboTicks, out Track.Piece turboStartPiece, out Track.Piece turboEndPiece) {

            turboStartPiece = null;
            turboEndPiece = null;

            double turboDistance = Math.Max(10, Math.Min(20, physicsParams.maxSpeed)) * turboTicks * 3 / 2;

            double bestCurvature = double.MaxValue;

            double startSearchDistance = 0.0;
            Track.Piece startPiece = searchStartPiece;

            while (true) {

                double distance = 0.0;
                double curvature = 0.0;

                Track.Piece currentPiece = startPiece;
                while (true) {
                    double pieceLength = currentPiece.GetLaneLength(0.0);
                    distance += pieceLength;

                    if (currentPiece.isBend) {
                        curvature += (1.0f / track.pieces[currentPiece.pieceIndex].radius) * pieceLength;
                    }

                    if (distance >= turboDistance) break;

                    currentPiece = track.GetNextPiece(currentPiece);
                    if (currentPiece == startPiece) break;
                }

                if (curvature < bestCurvature) {
                    bestCurvature = curvature;
                    turboStartPiece = startPiece;
                    turboEndPiece = track.GetNextPiece(currentPiece);
                }


                startSearchDistance += startPiece.GetLaneLength(0.0);
                startPiece = track.GetNextPiece(startPiece);

                if (startPiece == searchStartPiece || startSearchDistance > 0.85 * track.shortestLapLength) break;
            }

            return true;
        }

        public void GetFirstBendRange(Track.Piece startPiece, out Track.Piece firstBendStart, out Track.Piece firstBendEnd) {
            Track.Piece currentPiece = startPiece;

            firstBendStart = firstBendEnd = null;

            while (true) {
                if (firstBendStart == null) {
                    if (currentPiece.isBend) {
                        firstBendStart = currentPiece;
                    }
                }
                else {
                    if (!currentPiece.isBend || currentPiece.angle * firstBendStart.angle < 0) {
                        firstBendEnd = currentPiece;
                        break;
                    }
                }

                currentPiece = track.GetNextPiece(currentPiece);
                if (currentPiece == startPiece) break;
            }
        }

        public double GetDistanceBetweenPieces(Track.Piece startPiece, Track.Piece endPiece, TrackRoute route) {
            Track.Piece currentPiece = startPiece;

            double distance = 0.0;

            while (currentPiece != endPiece) {
                TrackRoute.Piece routePiece = route.pieces[currentPiece.pieceIndex];
                if (routePiece.startLaneIndex < 0 || routePiece.endLaneIndex < 0) break;

                distance += currentPiece.GetPieceLength(routePiece.startLaneIndex, routePiece.endLaneIndex);

                currentPiece = track.GetNextPiece(currentPiece);
            }

            return distance;
        }

        public Track.Piece GetNextSwitchPiece(Track.Piece startPiece) {
            Track.Piece currentPiece = track.GetNextPiece(startPiece);

            while (currentPiece != startPiece) {
                if (currentPiece.isSwitch) return currentPiece;
                currentPiece = track.GetNextPiece(currentPiece);
            }

            return null;
        }

        public TrackRoute GetShortestRoute(int startPieceIndex, int startLaneIndex, double maxDistance, int firstSwitchChoice) {
            TrackRoute route = new TrackRoute(track.pieces.Length);

            Track.Piece startPiece = track.pieces[startPieceIndex];
            Track.Piece currentPiece = startPiece;
            int currentLaneIndex = startLaneIndex;
            double currentDistance = 0.0;

            bool firstSwitch = true;

            while (currentDistance < maxDistance) {
                TrackRoute.Piece routePiece = route.pieces[currentPiece.pieceIndex];

                routePiece.startLaneIndex = currentLaneIndex;

                if (currentPiece.isSwitch) {

                    var laneLengths = new List<KeyValuePair<int, double>>();

                    for (int laneIndex = currentLaneIndex - 1; laneIndex <= currentLaneIndex + 1; ++laneIndex) {
                        if (laneIndex < 0 || laneIndex >= track.lanes.Length) continue;

                        double length = currentPiece.GetPieceLength(currentLaneIndex, laneIndex) + track.GetNextPiece(currentPiece).lanes[laneIndex].distanceFromStartToLapEnd;

                        laneLengths.Add(new KeyValuePair<int, double>(laneIndex, length));
                    }

                    laneLengths.Sort((KeyValuePair<int, double> a, KeyValuePair<int, double> b) => {
                        return a.Value.CompareTo(b.Value);
                    });

                    int choice = 0;
                    if (firstSwitch) {
                        firstSwitch = false;
                        choice = firstSwitchChoice;
                    }

                    currentLaneIndex = laneLengths[Math.Min(laneLengths.Count - 1, choice)].Key;
                }

                routePiece.endLaneIndex = currentLaneIndex;

                currentDistance += currentPiece.GetPieceLength(routePiece.startLaneIndex, routePiece.endLaneIndex);

                currentPiece = track.GetNextPiece(currentPiece);
                if (currentPiece == startPiece) break;
            }

            return route;
        }
    }
}
