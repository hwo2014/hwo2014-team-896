using System;

namespace Bot {
	public static class DegreesMath {
		public static double Deg2Rad = Math.PI / 180.0;
		public static double Rad2Deg = 180.0 / Math.PI;

		public static double Cos(double degrees) { return Math.Cos(Deg2Rad * degrees); }
		public static double Sin(double degrees) { return Math.Sin(Deg2Rad * degrees); }
	}

	public static class MathUtils {
		public static double Lerp(double from, double to, double t) { return from + (to - from) * t; }
        public static double Clamp(double value, double min, double max) { return Math.Max(min, Math.Min(max, value)); }
        public static double Clamp01(double value) { return Clamp(value, 0, 1); }
	}

	public struct Vector2 {
		public double x;
		public double y;

		public Vector2(double x, double y) { this.x = x; this.y = y; }

		public static Vector2 operator+ (Vector2 a, Vector2 b) { return new Vector2(a.x + b.x, a.y + b.y); }
		public static Vector2 operator- (Vector2 a, Vector2 b) { return new Vector2(a.x - b.x, a.y - b.y); }

		public static Vector2 operator* (Vector2 a, double s) { return new Vector2(a.x * s, a.y * s); }
		public static Vector2 operator/ (Vector2 a, double s) { return new Vector2(a.x / s, a.y / s); }

		public static double Dot(Vector2 a, Vector2 b) { return a.x * b.x + a.y * b.y; }

		public double SqrLength() { return x * x + y * y; }
		public double Length() { return Math.Sqrt(x * x + y * y); }

		public Vector2 Normalized() { return this / Length(); }

		public Vector2 NormalPositive() { return new Vector2(-y, x); }
		public Vector2 NormalNegative() { return new Vector2(y, -x); }

		public static Vector2 FromAngle(double angle) { return new Vector2(DegreesMath.Sin(angle), -DegreesMath.Cos(angle)); }

		public double Angle() { return Math.Atan2(x, -y) * DegreesMath.Rad2Deg; }
        public static double AngleBetween(Vector2 a, Vector2 b) { return Math.Acos(Vector2.Dot(a.Normalized(), b.Normalized())) * DegreesMath.Rad2Deg; }

		public Vector2 Rotated(double angle) {
			//x' = x cos f - y sin f
			//y' = y cos f + x sin f
			double c = DegreesMath.Cos(angle);
			double s = DegreesMath.Sin(angle);
			return new Vector2(x * c - y * s, y * c + x * s);
		}

		public Vector2 Abs() { return new Vector2(Math.Abs(x), Math.Abs(y)); }
		public static Vector2 Max(Vector2 a, Vector2 b) { return new Vector2(Math.Max(a.x, b.x), Math.Max(a.y, b.y)); }
		public static Vector2 Min(Vector2 a, Vector2 b) { return new Vector2(Math.Min(a.x, b.x), Math.Min(a.y, b.y)); }

        public static Vector2 Lerp(Vector2 a, Vector2 b, double t) { return a + (b - a) * t; }

        public static bool LineIntersection(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, out Vector2 pos) {

            pos = new Vector2();

            double c = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);
            if (Math.Abs(c) < 0.00001) return false; // Parallel lines

            double posA = ((b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x)) / c;
            //if (posA < 0.0f || posA > 1.0f) return false;

            //double posB = ((a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x)) / c;
            //if (posB < 0.0f || posB > 1.0f) return false;

            pos = Vector2.Lerp(a1, a2, posA);

            return true;
        }
	}

	public struct QuadBezier {
		public Vector2 p0;
		public Vector2 p1;
		public Vector2 p2;

		public QuadBezier(Vector2 p0, Vector2 p1, Vector2 p2) {
			this.p0 = p0;
			this.p1 = p1;
			this.p2 = p2;
		}

		public Vector2 Evaluate(double t) {
			return 
				p0 * ((1 - t) * (1 - t)) +
				p1 * (2 * (1 - t) * t) +
				p2 * (t * t);
		}

		public Vector2 EvaluateDerivative(double t) {
			return 
				(p1 - p0) * (2 * (1 - t)) + 
				(p2 - p1) * (2 * t);
		}

		public Vector2 EvaluateSecondDerivative(double t) {
			return p2 * 2 - p1 * 4 + p0 * 2;
		}

		public double EvaluateRadius(double t) {
			Vector2 d1 = EvaluateDerivative(t);
			Vector2 d2 = EvaluateSecondDerivative(t);

			double r1 = Math.Sqrt(Math.Pow(d1.x * d1.x + d1.y * d1.y, 3));
			double r2 = Math.Abs(d1.x * d2.y - d2.x * d1.y);
			return r1 / r2;
		}
	}
}

