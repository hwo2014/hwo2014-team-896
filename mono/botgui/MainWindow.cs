using System;
using Gtk;
using Cairo;
using Bot;

public partial class MainWindow: Gtk.Window {
	public Bot.Track track = null;
	public CarPosition[] carPositions = null;
	public CarState simulationResult = null;
	public Bot.Track.Piece turboStartPiece = null;
	public Bot.Track.Piece turboEndPiece = null;
	double trackMargin = 40.0;
	double trackScale = 1.0;

	public MainWindow (): base (Gtk.WindowType.Toplevel) {
		Build();
	}

	public void Redraw() {
		drawingarea1.QueueDraw();
	}

	protected void OnDeleteEvent(object sender, DeleteEventArgs a) {
		Application.Quit();
		a.RetVal = true;
	}

	protected void OnExpose(object o, ExposeEventArgs args) {
		DrawingArea area = (DrawingArea)o;
		Cairo.Context context =  Gdk.CairoHelper.Create(area.GdkWindow);

		if (track != null) {
			trackScale = Math.Min(Allocation.Width / (track.boundsMax.x - track.boundsMin.x + 2.0 * trackMargin), 
			                      Allocation.Height / (track.boundsMax.y - track.boundsMin.y + 2.0 * trackMargin));

			context.LineWidth = 1;
			context.NewPath();
			context.SetSourceRGB(0.8, 0.8, 0.8);
			for (int i=0; i<track.lanes.Length - 1; ++i) {
				foreach (Bot.Track.Piece piece in track.pieces) {
					Vector2 startNormal = Vector2.FromAngle(piece.startAngle).NormalPositive();
					context.MoveTo(PointToWindow(piece.startPoint + startNormal * track.lanes[i].distanceFromCenter));
					context.LineTo(PointToWindow(piece.startPoint + startNormal * track.lanes[i + 1].distanceFromCenter));
				}
			}
			context.StrokePreserve();

            context.SetSourceRGB(0.8, 0.8, 0.8);
            foreach (Bot.Track.Piece piece in track.pieces) {
                context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
                context.SetFontSize(10);
                
                string text = piece.pieceIndex.ToString();

                TextExtents s = context.TextExtents(text);
                PointD p = PointToWindow(piece.EvalutePosition(0.5, track.lanes[track.lanes.Length - 1].distanceFromCenter + 20));

                context.MoveTo(p.X - s.Width * 0.5, p.Y + s.Height * 0.5);
                context.ShowText(text);
            }

			if (turboStartPiece != null) {
				context.LineWidth = 3;
				context.NewPath();
				context.SetSourceRGB(1.0, 0.0, 0.0);

				Vector2 startNormal = Vector2.FromAngle(turboStartPiece.startAngle).NormalPositive();
				for (int i=0; i<track.lanes.Length - 1; ++i) {
					context.MoveTo(PointToWindow(turboStartPiece.startPoint + startNormal * track.lanes[i].distanceFromCenter));
					context.LineTo(PointToWindow(turboStartPiece.startPoint + startNormal * track.lanes[i + 1].distanceFromCenter));
				}

				context.StrokePreserve();
			}
			
			if (turboEndPiece != null) {
				context.LineWidth = 3;
				context.NewPath();
				context.SetSourceRGB(0.0, 0.0, 1.0);

				Vector2 startNormal = Vector2.FromAngle(turboEndPiece.startAngle).NormalPositive();
				for (int i=0; i<track.lanes.Length - 1; ++i) {
					context.MoveTo(PointToWindow(turboEndPiece.startPoint + startNormal * track.lanes[i].distanceFromCenter));
					context.LineTo(PointToWindow(turboEndPiece.startPoint + startNormal * track.lanes[i + 1].distanceFromCenter));
				}

				context.StrokePreserve();
			}

			context.LineWidth = 1;
			context.NewPath();
			context.SetSourceRGB(0.5, 0.5, 0.5);
			for (int i=0; i<track.lanes.Length - 1; ++i) {
				foreach (Bot.Track.Piece piece in track.pieces) {
					if (piece.isSwitch) {
						Vector2 startNormal = Vector2.FromAngle(piece.startAngle).NormalPositive();
						Vector2 endNormal = Vector2.FromAngle(piece.endAngle).NormalPositive();

						context.MoveTo(PointToWindow(piece.startPoint + startNormal * track.lanes[i].distanceFromCenter));
						context.LineTo(PointToWindow(piece.endPoint + endNormal * track.lanes[i + 1].distanceFromCenter));

						context.MoveTo(PointToWindow(piece.startPoint + startNormal * track.lanes[i + 1].distanceFromCenter));
						context.LineTo(PointToWindow(piece.endPoint + endNormal * track.lanes[i].distanceFromCenter));
					}
				}
			}
			context.StrokePreserve();

			foreach (Bot.Track.Lane lane in track.lanes) {
				context.NewPath();
				if (lane.index == 0) {
					context.SetSourceRGB(1, 0, 0);
				} else {
					context.SetSourceRGB(0, 0, 1);
				}

				foreach (Bot.Track.Piece piece in track.pieces) {

					Vector2 startPoint = piece.startPoint + Vector2.FromAngle(piece.startAngle).NormalPositive() * lane.distanceFromCenter;
					Vector2 endPoint = piece.endPoint + Vector2.FromAngle(piece.endAngle).NormalPositive() * lane.distanceFromCenter;

					context.MoveTo(PointToWindow(startPoint));

					if (piece.isBend) {
						PointD c = PointToWindow(piece.bendArcCenter);
						double angleOffset = piece.angle < 0 ? 90 : -90;
						double angle1 = Bot.DegreesMath.Deg2Rad * (piece.startAngle + angleOffset - 90);
						double angle2 = Bot.DegreesMath.Deg2Rad * (piece.endAngle + angleOffset - 90);

						double radius = (piece.radius + (piece.angle < 0 ? lane.distanceFromCenter : -lane.distanceFromCenter)) * trackScale;

						if (piece.angle < 0) {
							context.MoveTo(PointToWindow(endPoint));
							context.Arc(c.X, c.Y, radius, angle2, angle1);
							context.MoveTo(PointToWindow(endPoint));
						} else {
							context.Arc(c.X, c.Y, radius, angle1, angle2);
						}
					} else {
						context.LineTo(PointToWindow(endPoint));
					}
				}

				context.StrokePreserve();
			}

			if (carPositions != null) {
				foreach (CarPosition carPosition in carPositions) {
                    DrawCar(context, carPosition, 4, 0, 0, 0);
				}
			}

            if (simulationResult != null) {
                CarState[] states = simulationResult.ToArray();

				for (int i=10; i < states.Length; i += 10) {

					Bot.CarState state = states[i];

                    CarPosition carPosition = new CarPosition() {
                        angle = state.angle,
                        piecePosition = new PiecePosition() {
                            pieceIndex = state.piece.pieceIndex,
                            inPieceDistance = state.inPieceDistance,
                            lap = 0,
                            lane = new PiecePosition.Lane() {
                                startLaneIndex = state.startLaneIndex,
                                endLaneIndex = state.endLaneIndex
                            }
                        }
                    };

                    DrawCar(context, carPosition, 2, 0.5, 0.5, 1.0);
                }
            }
		}

		((IDisposable)context.Target).Dispose();                                      
		((IDisposable)context).Dispose();
	}

    void DrawCar(Cairo.Context context, CarPosition carPosition, double width, double r, double g, double b) {
        Vector2 point = carPosition.piecePosition.GetTrackPoint(track);
        double angle = carPosition.GetAbsoluteAngle(track);

        context.LineWidth = width;
        context.SetSourceRGB(r, g, b);
        context.NewPath();

        PointD c = PointToWindow(point - Vector2.FromAngle(angle) * 15.0);
        PointD d = PointToWindow(point + Vector2.FromAngle(angle) * 15.0);

        context.MoveTo(c);
        context.LineTo(d);

        context.StrokePreserve();
    }

	PointD PointToWindow(Vector2 point) {
		Vector2 p = (new Vector2(trackMargin, trackMargin) + point - track.boundsMin) * trackScale;
		return new PointD(p.x, p.y);
	}
	
	protected void OnQuit(object sender, EventArgs e) {
		Application.Quit();
	}
}
