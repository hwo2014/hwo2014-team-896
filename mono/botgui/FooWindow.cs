using System;
using Gdk;
using Gtk;
using Cairo;
using Bot;

namespace botgui {
	public partial class FooWindow : Gtk.Window {

		Vector2 center = new Vector2(10, 10);

        double controlPointAngle = 35;
        double controlPointDistance = 100;

		double radius0 = 70;
		double radius1 = 50;
		double angle = 80;

        double[] data = new double[] {
				0.05, 130.30,
				0.13, 111.18,
				0.21, 96.29,
				0.28, 81.48,
				0.35, 70.56,
				0.43, 61.64,
				0.50, 54.81,
				0.57, 50.16,
				0.63, 47.76,
				0.70, 47.50,
				0.76, 49.32,
				0.83, 52.67,
				0.89, 58.63,
				0.95, 65.43,
			};

        /*double radius0 = 80;
        double radius1 = 60;
        double angle = 45;

        double[] data = new double[] {
            0.06943218222,107.00477,
            0.1364430669,98.36956,
            0.1975213024,91.55992,
            0.2549034054,85.30966,
            0.3155953272,79.62608,
            0.3759535836,74.53429,
            0.4313078819,70.03784,
            0.4871173807,66.75785,
            0.5447063153,63.39261,
            0.5960578233,61.07058,
            0.6558366092,58.86972,
            0.7028195254,57.53208,
            0.7647444031,57.53208,
            0.8074460755,56.2156,
            0.865333634,56.30238,
            0.9100938793,56.87656,
            0.9760688965,58.16626,
			};*/


		public FooWindow () : 
				base(Gtk.WindowType.Toplevel) {
			this.Build();
		}

		protected void OnDeleteEvent(object sender, DeleteEventArgs a) {
			Application.Quit();
			a.RetVal = true;
		}

		protected void OnQuitEvent(object sender, EventArgs e) {
			Application.Quit();
		}

		public void Redraw() {
			drawingarea2.QueueDraw();
		}

		QuadBezier MakeBezier() {
			return new QuadBezier(center + new Vector2(radius0, 0.0), 
			       		          center + Vector2.FromAngle(90 + controlPointAngle) * controlPointDistance, 
			                      center + Vector2.FromAngle(90 + angle) * radius1);
		}

        double CalculateError() {
            QuadBezier bezier = MakeBezier();

            double error = 0.0;

            for (int i = 0; i < data.Length / 2; ++i) {
                double t = data[2 * i];
                double r = data[2 * i + 1];
                double r2 = bezier.EvaluateRadius(t);

                error += (r2 - r) * (r2 - r);
            }

            return error;
        }

        void OptimizePoint() {
            double angleStep = 0.001;
            double distanceStep = 0.001;

            for (int i = 0; i < 2000; ++i) {

                double originalAngle = controlPointAngle;
                double originalDistance = controlPointDistance;
                double originalError = CalculateError();

                double minAngle = originalAngle;
                double minDistance = originalDistance;
                double minError = originalError;

                double error;

                controlPointAngle = Math.Max(0, Math.Min(angle, originalAngle - angleStep));
                error = CalculateError();
                if (error < minError) {
                    minError = error;
                    minAngle = controlPointAngle;
                }

                controlPointAngle = Math.Max(0, Math.Min(angle, originalAngle + angleStep));
                error = CalculateError();
                if (error < minError) {
                    minError = error;
                    minAngle = controlPointAngle;
                }

                controlPointAngle = minAngle;

                controlPointDistance = Math.Max(radius0 * 0.5, Math.Min(radius1 * 2, originalDistance - distanceStep));
                error = CalculateError();
                if (error < minError) {
                    minError = error;
                    minDistance = controlPointDistance;
                }

                controlPointDistance = Math.Max(radius0 * 0.5, Math.Min(radius1 * 2, originalDistance + distanceStep));
                error = CalculateError();
                if (error < minError) {
                    minError = error;
                    minDistance = controlPointDistance;
                }

                controlPointDistance = minDistance;
            }
        }

        protected void OnButtonPress(object o, ButtonPressEventArgs args) {
			//Console.WriteLine(args.Event.X + " " + args.Event.Y);

			Vector2 p = WindowToPoint(new PointD(args.Event.X, args.Event.Y));

			controlPointAngle = (p - center).Angle() - 90;
			controlPointDistance = (p - center).Length();

            //OptimizePoint();

			Console.WriteLine("Angle: {0:0.00}, Radius: {1:0.00}", controlPointAngle, controlPointDistance);

			QuadBezier bezier = MakeBezier();

			for (int i=0; i<data.Length / 2; ++i) {
				double t = data[2 * i];
				Console.Write("{0:###.0} ", bezier.EvaluateRadius(t));
			}
			Console.WriteLine();

			for (int i=0; i<data.Length / 2; ++i) {
				double r = data[2 * i + 1];
				Console.Write("{0:###.0} ", r);
			}
			Console.WriteLine();

			Redraw();
		}

		protected void OnExpose(object o, ExposeEventArgs args) {
			DrawingArea area = (DrawingArea)o;
			Cairo.Context context =  Gdk.CairoHelper.Create(area.GdkWindow);

			context.LineWidth = 1;
			context.SetSourceRGB(0, 0, 0);


			context.NewPath();
			DrawArc(context, center, radius0, 0, angle);
			context.StrokePreserve();

			context.NewPath();
			DrawArc(context, center, radius1, 0, angle);
			context.StrokePreserve();

			QuadBezier bezier = MakeBezier();

			context.SetSourceRGB(0.7, 0, 0);
			context.NewPath();
			context.MoveTo(PointToWindow(bezier.Evaluate(0)));
			for (int i=1; i<=100; ++i) {
				double t = i / 100.0;
				//if (i % 10 == 0) Console.WriteLine("{0:0.000}: {1:0.000}", t, bezier.EvaluateRadius(t));
				context.LineTo(PointToWindow(bezier.Evaluate(t)));
			}
	        context.StrokePreserve();

			context.SetSourceRGB(0.0, 0.7, 0);
			context.NewPath();
			context.MoveTo(PointToWindow(center + Vector2.FromAngle(90) * radius0));
			for (int i=1; i<=100; ++i) {
				double t = i / 100.0;
				context.LineTo(PointToWindow(center + Vector2.FromAngle(90 + t * angle) * MathUtils.Lerp(radius0, radius1, t)));
			}
			context.StrokePreserve();

			context.SetSourceRGB(0.7, 0.4, 0.4);
			context.NewPath();
			context.MoveTo(PointToWindow(bezier.p0));
			context.LineTo(PointToWindow(bezier.p1));
			context.LineTo(PointToWindow(bezier.p2));
			context.StrokePreserve();

			((IDisposable)context.Target).Dispose();                                      
			((IDisposable)context).Dispose();
		}

		void DrawArc(Cairo.Context context, Vector2 center, double radius, double angle1, double angle2) {
			PointD p = PointToWindow(center);
			context.Arc(p.X, p.Y, SizeToWindow(radius), DegreesMath.Deg2Rad * angle1, DegreesMath.Deg2Rad * angle2);
		}

		PointD PointToWindow(Vector2 point) {
			Vector2 p = point * 3;
			return new PointD(p.x, p.y);
		}

		Vector2 WindowToPoint(PointD p) {
			Vector2 point = new Vector2(p.X, p.Y);
			return point / 3;
		}

		double SizeToWindow(double size) {
			return size * 3;
		}
	}
}

