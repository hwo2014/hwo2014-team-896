using System;
using System.Collections.Generic;
using Gtk;
using System.Threading;
using System.IO;
using System.Net.Sockets;

namespace botgui {
	class MainClass : Bot.IBotVisualizer {

		class BotRunner {
			public bool master;
			public string botName;
			public int joinDelay;
			public Bot.Bot bot;
			public Thread botThread;
		}

		MainWindow window;

		List<BotRunner> botRunners = new List<BotRunner>();

		public static void Main(string[] args) {
			Application.Init();
			MainClass main = new MainClass();
			main.RunBots();
		}

		string password;

		void RunTest() {
			FooWindow window = new FooWindow();
			window.KeepAbove = true;
			window.Show();

			Application.Run();
		}

		void RunBots() {
			window = new MainWindow();
			//window.KeepAbove = true;
			window.Show();

			password = "suchspeed" + new Random().Next();
            //password = "kei";
			Console.WriteLine(password);

			Bot.Bot mainBot = new Bot.Bot() { writeData = false, algorithm = Bot.Bot.Algorithm.Good, startDelayTicks = 0 };
			mainBot.visualizers.Add(this);

            for (int i = 2; i <= 1; ++i) {
                botRunners.Add(new BotRunner() {
                    master = false,
                    botName = "Speed " + i,
                    bot = new Bot.Bot() {
                        enableLogging = false,
                        writeData = false,
                        startDelayTicks = 0 * botRunners.Count,
                        algorithm = Bot.Bot.Algorithm.Greedy,
                        crashyCrashInterval = 100 + (i - 2) * 80,
                        botSpeed = 1.0 - 0.0 - (i - 2) * 0.0,
                    }
                });
            }
            botRunners.Add(new BotRunner() { master = false, botName = "Speed 1", bot = mainBot });

			for (int i=0; i<botRunners.Count; ++i) {
				BotRunner runner = botRunners[i];
				runner.joinDelay = 2000 + 2000 * i;
				BotRunner r = runner;
				runner.botThread = new Thread(() => { RunBot(r, botRunners.Count); });
				runner.botThread.Start();
			}

			Application.Run();

			foreach (BotRunner runner in botRunners) {
				runner.bot.stop = true;
			}
		}

		public void OnBotTrackUpdate(Bot.Bot bot, Bot.Track track) {
			Gtk.Application.Invoke(delegate {
				window.track = track;
				window.Redraw();
			});
		}

		public void OnBotCarsUpdate(Bot.Bot bot, Bot.CarPosition[] carPositions) {
			Gtk.Application.Invoke(delegate {
				window.carPositions = carPositions;
				window.Redraw();
			});
		}

        public void OnBotSimulationUpdate(Bot.Bot bot, Bot.CarState result) {
            Gtk.Application.Invoke(delegate {
                window.simulationResult = result;
                window.Redraw();
            });
        }

		public void OnBotTurboUpdate(Bot.Bot bot, Bot.Track.Piece turboStartPiece, Bot.Track.Piece turboEndPiece) {
			Gtk.Application.Invoke(delegate {
				window.turboStartPiece = turboStartPiece;
				window.turboEndPiece = turboEndPiece;
				window.Redraw();
			});
		}

		void RunBot(BotRunner runner, int numBots) {
			Thread.Sleep(runner.joinDelay);

			string host = "prost.helloworldopen.com";
			int port = 8091;
			string botKey = "ErNwnGH48vGawQ";
            string trackName = "keimola";

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + runner.botName + "/" + botKey);

			using(TcpClient client = new TcpClient(host, port)) {
                client.NoDelay = true;

				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;

                //numBots = 8;
			
				//bot.Join(botName, botKey);
				runner.bot.SetStreams(reader, writer);
				if (runner.master) {
					runner.bot.CreateRace(runner.botName, botKey, trackName, password, numBots);
				} else {
					runner.bot.JoinRace(runner.botName, botKey, trackName, password, numBots);
				}
				runner.bot.Run();
			}
		}
	}
}
