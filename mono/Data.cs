using System;
using System.Collections.Generic;
namespace Bot {
    class Data {
        public static void FillData(DataHandler handler) {
            {
                BendSwitchData data = new BendSwitchData() { angle = 22, r0 = 180, r1 = 200,
                    rawLengths = new List<double>(new double[] {
                        77.255183366416, 77.255183366417, 77.255183366417, 77.255183366418, 77.255183366418, 77.255183366418, 77.255183366418, 77.255183366419, 77.255183366419, 77.255183366421, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(0.87611950201, 168.280702795859 ),
                        new KeyValuePair<double, double>(2.263380816689, 168.330377132374 ),
                        new KeyValuePair<double, double>(10.424628390755, 169.578205068815 ),
                        new KeyValuePair<double, double>(12.987318536027, 170.141205397109 ),
                        new KeyValuePair<double, double>(20.022404169933, 172.40386550182 ),
                        new KeyValuePair<double, double>(25.722211773497, 175.13431009128 ),
                        new KeyValuePair<double, double>(29.548555038476, 177.180181747478 ),
                        new KeyValuePair<double, double>(35.054953230545, 180.485359720893 ),
                        new KeyValuePair<double, double>(39.80782975971, 183.72459298969 ),
                        new KeyValuePair<double, double>(46.266225308878, 188.621410203309 ),
                        new KeyValuePair<double, double>(50.014058843734, 192.017267312843 ),
                        new KeyValuePair<double, double>(55.625390204227, 197.973508986474 ),
                        new KeyValuePair<double, double>(59.619808945645, 202.02686404801 ),
                        new KeyValuePair<double, double>(66.75150268344, 209.952199545745 ),
                        new KeyValuePair<double, double>(69.949854402918, 213.73092744132 ),
                    }),
                    polynomialConstant = 168.13380720396,
                    polynomialLinear = 0.0475065897227782,
                    polynomialQuadratic = 0.00864985131184544,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 22, r0 = 190, r1 = 210,
                    rawLengths = new List<double>(new double[] {
                        81.054652206371, 81.054652206374, 81.054652206375, 81.054652206375, 81.054652206375, 81.054652206375, 81.054652206375, 81.054652206375, 81.054652206375, 81.054652206375, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(1.374066996823, 178.598486564233 ),
                        new KeyValuePair<double, double>(4.465530418892, 178.804919238277 ),
                        new KeyValuePair<double, double>(9.59726528054, 179.44515475522 ),
                        new KeyValuePair<double, double>(15.500054569091, 180.915881410334 ),
                        new KeyValuePair<double, double>(17.919958347675, 181.649648287132 ),
                        new KeyValuePair<double, double>(21.579532411295, 182.779403610217 ),
                        new KeyValuePair<double, double>(25.71033201129, 184.440229164721 ),
                        new KeyValuePair<double, double>(29.103694382681, 185.965327771496 ),
                        new KeyValuePair<double, double>(34.697126415564, 189.051761243889 ),
                        new KeyValuePair<double, double>(38.042962796147, 191.053960178001 ),
                        new KeyValuePair<double, double>(40.899756196333, 193.22955865497 ),
                        new KeyValuePair<double, double>(46.65753936821, 197.448409601459 ),
                        new KeyValuePair<double, double>(55.820037432412, 205.136867921251 ),
                        new KeyValuePair<double, double>(62.462093116531, 212.372302097972 ),
                        new KeyValuePair<double, double>(72.729038402552, 223.333220338764 ),
                    }),
                    polynomialConstant = 178.484816204231,
                    polynomialLinear = 0.0282655178998106,
                    polynomialQuadratic = 0.0081155972269132,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 22, r0 = 200, r1 = 180,
                    rawLengths = new List<double>(new double[] {
                        77.254437893182, 77.254437893183, 77.254437893184, 77.254437893184, 77.254437893184, 77.254437893184, 77.254437893184, 77.254437893184, 77.254437893184, 77.254437893185, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(2.04744415351, 221.739800410611 ),
                        new KeyValuePair<double, double>(3.787233883679, 219.675295370814 ),
                        new KeyValuePair<double, double>(11.214132778443, 209.932955557421 ),
                        new KeyValuePair<double, double>(15.813858702825, 204.559018076808 ),
                        new KeyValuePair<double, double>(22.06459699974, 197.957490504774 ),
                        new KeyValuePair<double, double>(26.012662484294, 91.510209784684 ),
                        new KeyValuePair<double, double>(31.696762171274, 188.608300598641 ),
                        new KeyValuePair<double, double>(35.674413739075, 185.471250266326 ),
                        new KeyValuePair<double, double>(41.854755453909, 180.986884382716 ),
                        new KeyValuePair<double, double>(45.613908322414, 178.525720297845 ),
                        new KeyValuePair<double, double>(51.615338734113, 175.514739013539 ),
                        new KeyValuePair<double, double>(55.590707745199, 173.677972721713 ),
                        new KeyValuePair<double, double>(61.538145256486, 171.287053287093 ),
                        new KeyValuePair<double, double>(66.55281949219, 169.752059972432 ),
                        new KeyValuePair<double, double>(73.515721116003, 82.531349236237 ),
                    }),
                    polynomialConstant = 217.041175591411,
                    polynomialLinear = -1.49305207796078,
                    polynomialQuadratic = 0.00986561636159769,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 22, r0 = 200, r1 = 220,
                    rawLengths = new List<double>(new double[] {
                        84.865783392973, 84.865783392974, 84.865783392974, 84.865783392975, 84.865783392975, 84.865783392976, 84.865783392976, 84.865783392976, 84.865783392976, 84.865783392976, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(1.687163134832, 188.944206313754 ),
                        new KeyValuePair<double, double>(5.368818386186, 189.156605460632 ),
                        new KeyValuePair<double, double>(12.815827522337, 190.259218921032 ),
                        new KeyValuePair<double, double>(17.17817899473, 191.271366637151 ),
                        new KeyValuePair<double, double>(23.506600244743, 193.166277365711 ),
                        new KeyValuePair<double, double>(27.555571308662, 194.860378451468 ),
                        new KeyValuePair<double, double>(34.153976388677, 198.158227411906 ),
                        new KeyValuePair<double, double>(40.837249827576, 202.17774011249 ),
                        new KeyValuePair<double, double>(45.0268657707, 205.054115747279 ),
                        new KeyValuePair<double, double>(52.036009998605, 210.232758480485 ),
                        new KeyValuePair<double, double>(56.089095344977, 213.825259592887 ),
                        new KeyValuePair<double, double>(63.010576819358, 220.150377332875 ),
                        new KeyValuePair<double, double>(69.417385958829, 226.255764660193 ),
                        new KeyValuePair<double, double>(73.140652680276, 230.937262343316 ),
                        new KeyValuePair<double, double>(80.992485309548, 240.038618650621 ),
                    }),
                    polynomialConstant = 188.815789241689,
                    polynomialLinear = 0.00986824970756441,
                    polynomialQuadratic = 0.0076771294506874,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 22, r0 = 210, r1 = 190,
                    rawLengths = new List<double>(new double[] {
                        81.053904159303, 81.053904159305, 81.053904159305, 81.053904159305, 81.053904159305, 81.053904159305, 81.053904159305, 81.053904159305, 81.053904159305, 81.053904159306, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(3.210108437497, 231.445080501855 ),
                        new KeyValuePair<double, double>(17.152321314086, 214.080098857644 ),
                        new KeyValuePair<double, double>(24.94055200186, 206.655952263461 ),
                        new KeyValuePair<double, double>(33.244763737931, 198.737640413699 ),
                        new KeyValuePair<double, double>(37.565303703146, 195.564526408395 ),
                        new KeyValuePair<double, double>(41.982720740203, 192.658614899591 ),
                        new KeyValuePair<double, double>(46.490661002418, 189.526589979903 ),
                        new KeyValuePair<double, double>(52.196929153122, 186.36612775913 ),
                        new KeyValuePair<double, double>(56.119709141543, 184.433198587665 ),
                        new KeyValuePair<double, double>(60.925950853478, 182.475078391175 ),
                        new KeyValuePair<double, double>(64.742512623627, 181.388808741135 ),
                        new KeyValuePair<double, double>(67.68810096176, 180.477747790692 ),
                        new KeyValuePair<double, double>(72.024702613634, 179.587371511104 ),
                        new KeyValuePair<double, double>(74.986817959258, 179.073466270379 ),
                        new KeyValuePair<double, double>(77.447710430604, 178.803494468931 ),
                    }),
                    polynomialConstant = 235.24726240024,
                    polynomialLinear = -1.35933961997327,
                    polynomialQuadratic = 0.00813288599170419,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 22, r0 = 220, r1 = 200,
                    rawLengths = new List<double>(new double[] {
                        84.865033110328, 84.865033110328, 84.865033110329, 84.865033110329, 84.86503311033, 84.86503311033, 84.86503311033, 84.86503311033, 84.865033110331, 84.865033110331, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(2.360730731724, 243.24058940413 ),
                        new KeyValuePair<double, double>(7.788848323814, 235.868430092429 ),
                        new KeyValuePair<double, double>(13.742202440586, 229.013694347192 ),
                        new KeyValuePair<double, double>(18.599602947193, 224.44060392806 ),
                        new KeyValuePair<double, double>(23.887361474476, 218.489338602069 ),
                        new KeyValuePair<double, double>(29.605068390627, 213.810169825452 ),
                        new KeyValuePair<double, double>(35.316003780643, 208.85737443836 ),
                        new KeyValuePair<double, double>(40.850610500494, 204.443232895657 ),
                        new KeyValuePair<double, double>(45.433479881156, 201.624714790485 ),
                        new KeyValuePair<double, double>(51.431543303522, 198.149214547853 ),
                        new KeyValuePair<double, double>(56.310660152958, 195.609761581688 ),
                        new KeyValuePair<double, double>(62.309868675513, 193.16021241463 ),
                        new KeyValuePair<double, double>(68.453343003213, 191.266114059043 ),
                        new KeyValuePair<double, double>(72.453550180386, 190.255840425667 ),
                        new KeyValuePair<double, double>(78.808132166608, 189.319683172305 ),
                    }),
                    polynomialConstant = 246.216535454235,
                    polynomialLinear = -1.33458939105609,
                    polynomialQuadratic = 0.00776513940851964,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 100, r1 = 120,
                    rawLengths = new List<double>(new double[] {
                        88.65817365362, 88.658173653627, 88.658173653628, 88.658173653635, 88.658173653635, 88.658173653635, 88.658173653635, 88.658173653635, 88.658173653635, 88.658173653635, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(1.44866415643, 102.964615471129 ),
                        new KeyValuePair<double, double>(8.624887454694, 99.217956364539 ),
                        new KeyValuePair<double, double>(13.410585344746, 97.425436217207 ),
                        new KeyValuePair<double, double>(16.926870389284, 96.71874982981 ),
                        new KeyValuePair<double, double>(22.180270343475, 96.390934116503 ),
                        new KeyValuePair<double, double>(27.705818756183, 96.944001355258 ),
                        new KeyValuePair<double, double>(33.075845041084, 98.374578597078 ),
                        new KeyValuePair<double, double>(37.640484326792, 100.235515215034 ),
                        new KeyValuePair<double, double>(41.339746530443, 102.159966330878 ),
                        new KeyValuePair<double, double>(46.787534872367, 105.761498282778 ),
                        new KeyValuePair<double, double>(52.036131882222, 110.221578289572 ),
                        new KeyValuePair<double, double>(57.984675535953, 116.488784363372 ),
                        new KeyValuePair<double, double>(62.905655513107, 121.649307126222 ),
                        new KeyValuePair<double, double>(67.508820625011, 128.594510180355 ),
                        new KeyValuePair<double, double>(74.927025967917, 139.097373120231 ),
                    }),
                    polynomialConstant = 103.585843480181,
                    polynomialLinear = -0.65557124882233,
                    polynomialQuadratic = 0.0150870865132615,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 100, r1 = 80,
                    rawLengths = new List<double>(new double[] {
                        73.4468497816, 73.446849781614, 73.446849781618, 73.446849781625, 73.446849781627, 73.446849781627, 73.446849781627, 73.446849781627, 73.446849781627, 73.446849781627, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(0.899536802897, 138.237964791701 ),
                        new KeyValuePair<double, double>(7.401420442194, 125.103022251304 ),
                        new KeyValuePair<double, double>(11.37154404744, 118.425164018501 ),
                        new KeyValuePair<double, double>(15.37401463423, 112.210801897401 ),
                        new KeyValuePair<double, double>(20.819203479547, 103.244117096328 ),
                        new KeyValuePair<double, double>(24.831307842825, 98.263900399761 ),
                        new KeyValuePair<double, double>(29.637667829081, 92.108589554641 ),
                        new KeyValuePair<double, double>(33.884336594374, 87.610449585741 ),
                        new KeyValuePair<double, double>(39.116327578274, 83.28716707058 ),
                        new KeyValuePair<double, double>(44.064092428083, 80.384448431771 ),
                        new KeyValuePair<double, double>(47.748494850842, 78.232320858151 ),
                        new KeyValuePair<double, double>(53.008748312073, 76.679995991069 ),
                        new KeyValuePair<double, double>(59.005792943571, 76.177624340345 ),
                        new KeyValuePair<double, double>(62.375672776511, 76.436443170814 ),
                        new KeyValuePair<double, double>(67.254866393068, 77.705169719481 ),
                    }),
                    polynomialConstant = 140.801290525735,
                    polynomialLinear = -2.19036908168291,
                    polynomialQuadratic = 0.0185850898545703,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 110, r1 = 90,
                    rawLengths = new List<double>(new double[] {
                        80.847121638114, 81.028059516719, 81.028059516719, 81.028059516719, 81.028059516719, 81.028059516719, 81.028059516719, 81.028059516719, 81.028059516719, 81.02805951672, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(0.873412740477, 150.381915016422 ),
                        new KeyValuePair<double, double>(7.609431236393, 137.966393975754 ),
                        new KeyValuePair<double, double>(14.890515817528, 125.506120530893 ),
                        new KeyValuePair<double, double>(20.306761987381, 116.936401016954 ),
                        new KeyValuePair<double, double>(25.863661799348, 110.393190456292 ),
                        new KeyValuePair<double, double>(30.805523417119, 103.721945804521 ),
                        new KeyValuePair<double, double>(35.78934119409, 98.836910199167 ),
                        new KeyValuePair<double, double>(40.526326939232, 94.738720544638 ),
                        new KeyValuePair<double, double>(45.680424040511, 91.438555867492 ),
                        new KeyValuePair<double, double>(50.588846219321, 88.942078565884 ),
                        new KeyValuePair<double, double>(55.299199682704, 87.259856577172 ),
                        new KeyValuePair<double, double>(61.02308170738, 86.331812278338 ),
                        new KeyValuePair<double, double>(65.02429495541, 86.353246317956 ),
                        new KeyValuePair<double, double>(70.403657333874, 87.131894149387 ),
                        new KeyValuePair<double, double>(75.68416334167, 89.075859502042 ),
                    }),
                    polynomialConstant = 153.303540658565,
                    polynomialLinear = -2.11547451798948,
                    polynomialQuadratic = 0.0167139560570469,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 120, r1 = 100,
                    rawLengths = new List<double>(new double[] {
                        88.656740012432, 88.656740012432, 88.656740012434, 88.656740012435, 88.656740012435, 88.656740012435, 88.656740012435, 88.656740012435, 88.656740012435, 88.656740012436, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(4.421583295282, 157.401596583404 ),
                        new KeyValuePair<double, double>(14.784862688224, 139.067309314431 ),
                        new KeyValuePair<double, double>(22.155263407877, 128.568895700063 ),
                        new KeyValuePair<double, double>(29.741544834963, 118.465455258644 ),
                        new KeyValuePair<double, double>(37.348938368028, 110.205413567014 ),
                        new KeyValuePair<double, double>(43.724101396647, 105.089389836701 ),
                        new KeyValuePair<double, double>(48.368357199239, 102.149718814517 ),
                        new KeyValuePair<double, double>(51.614938224717, 100.22581864453 ),
                        new KeyValuePair<double, double>(57.754752229851, 97.794807056896 ),
                        new KeyValuePair<double, double>(62.577582083496, 96.787682386673 ),
                        new KeyValuePair<double, double>(65.308792795653, 96.475882013107 ),
                        new KeyValuePair<double, double>(70.264447325413, 96.51310561781 ),
                        new KeyValuePair<double, double>(75.576479237454, 97.430442930836 ),
                        new KeyValuePair<double, double>(80.220101715003, 98.865061154036 ),
                        new KeyValuePair<double, double>(83.944284644078, 100.449745371689 ),
                    }),
                    polynomialConstant = 166.001204242315,
                    polynomialLinear = -2.05372219781204,
                    polynomialQuadratic = 0.0151622965124514,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 60, r1 = 80,
                    rawLengths = new List<double>(new double[] {
                        58.503066752223, 58.503066752223, 58.503066752223, 58.503066752223, 58.503066752224, 58.503066752224, 58.503066752224, 58.503066752224, 58.503066752224, 58.503066752224, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(0.72026671925, 58.735191748144 ),
                        new KeyValuePair<double, double>(1.234761235256, 58.438930240672 ),
                        new KeyValuePair<double, double>(8.238817877352, 56.301092887554 ),
                        new KeyValuePair<double, double>(8.713879422463, 56.301092887558 ),
                        new KeyValuePair<double, double>(15.808744686261, 56.948947345138 ),
                        new KeyValuePair<double, double>(16.221701611709, 56.948947345112 ),
                        new KeyValuePair<double, double>(23.420050398844, 60.666531846537 ),
                        new KeyValuePair<double, double>(24.02878567987, 61.078607083079 ),
                        new KeyValuePair<double, double>(31.080816474942, 67.392548783733 ),
                        new KeyValuePair<double, double>(31.710890154638, 68.030195006973 ),
                        new KeyValuePair<double, double>(38.787104093381, 77.022822693942 ),
                        new KeyValuePair<double, double>(39.516626682119, 77.882218353601 ),
                        new KeyValuePair<double, double>(46.503004374335, 89.43784992588 ),
                        new KeyValuePair<double, double>(47.359074292328, 90.502822333347 ),
                        new KeyValuePair<double, double>(54.332620442512, 104.494357045281 ),
                    }),
                    polynomialConstant = 59.000535309614,
                    polynomialLinear = -0.522631392976963,
                    polynomialQuadratic = 0.0252392163815461,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 80, r1 = 100,
                    rawLengths = new List<double>(new double[] {
                        73.448262456319, 73.448262456319, 73.448262456319, 73.44826245632, 73.44826245632, 73.448262456321, 73.448262456321, 73.448262456321, 73.448262456321, 73.448262456321, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(1.149682833798, 80.412755521733 ),
                        new KeyValuePair<double, double>(2.364248390469, 79.608712283704 ),
                        new KeyValuePair<double, double>(8.166650253997, 77.232572066977 ),
                        new KeyValuePair<double, double>(9.615556550984, 76.850115683245 ),
                        new KeyValuePair<double, double>(15.242015189654, 76.17721125967 ),
                        new KeyValuePair<double, double>(17.613310793674, 76.207640189877 ),
                        new KeyValuePair<double, double>(23.314164157853, 77.222550577584 ),
                        new KeyValuePair<double, double>(24.319977619066, 77.687489298146 ),
                        new KeyValuePair<double, double>(29.996472630844, 79.981394549503 ),
                        new KeyValuePair<double, double>(31.941685424435, 81.278251691183 ),
                        new KeyValuePair<double, double>(37.493841928977, 85.648695502276 ),
                        new KeyValuePair<double, double>(44.475363284928, 92.124839802707 ),
                        new KeyValuePair<double, double>(50.928481391039, 100.217389211208 ),
                        new KeyValuePair<double, double>(54.249249503331, 104.322767175959 ),
                        new KeyValuePair<double, double>(64.277872346897, 121.067900628824 ),
                    }),
                    polynomialConstant = 80.888941182141,
                    polynomialLinear = -0.590582377901764,
                    polynomialQuadratic = 0.0189162039216519,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 80, r1 = 60,
                    rawLengths = new List<double>(new double[] {
                        58.501693202337, 58.501693202338, 58.501693202338, 58.501693202338, 58.501693202338, 58.501693202338, 58.501693202338, 58.501693202338, 58.501693202338, 58.501693202338, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(2.034352763554, 110.925167169748 ),
                        new KeyValuePair<double, double>(3.091279433482, 108.296742442723 ),
                        new KeyValuePair<double, double>(9.287751807486, 96.039874871596 ),
                        new KeyValuePair<double, double>(10.469858334311, 93.768375731757 ),
                        new KeyValuePair<double, double>(16.72426120821, 82.395681942749 ),
                        new KeyValuePair<double, double>(17.777089116466, 80.534911280212 ),
                        new KeyValuePair<double, double>(24.10038775829, 71.469878120405 ),
                        new KeyValuePair<double, double>(25.180104369622, 70.037839170085 ),
                        new KeyValuePair<double, double>(31.501167544687, 63.909857725689 ),
                        new KeyValuePair<double, double>(32.506019799644, 62.892652948261 ),
                        new KeyValuePair<double, double>(39.298785266641, 58.280508532767 ),
                        new KeyValuePair<double, double>(42.912432294527, 56.945287235217 ),
                        new KeyValuePair<double, double>(46.8118079731, 56.215604581617 ),
                        new KeyValuePair<double, double>(50.740140454232, 56.302376700738 ),
                        new KeyValuePair<double, double>(54.373307089292, 57.046252220368 ),
                    }),
                    polynomialConstant = 116.080248961308,
                    polynomialLinear = -2.44908970755999,
                    polynomialQuadratic = 0.0250599630705455,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 45, r0 = 90, r1 = 110,
                    rawLengths = new List<double>(new double[] {
                        80.429484142008, 81.029484142008, 81.029484142008, 81.029484142008, 81.029484142008, 81.029484142008, 81.029484142008, 81.029484142009, 81.029484142009, 81.029484142009, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(0.874280377834, 91.61265414052 ),
                        new KeyValuePair<double, double>(6.96136710835, 88.723694711992 ),
                        new KeyValuePair<double, double>(11.078742618997, 87.337303708511 ),
                        new KeyValuePair<double, double>(16.897098359657, 86.352088632173 ),
                        new KeyValuePair<double, double>(22.111972500665, 86.484689300627 ),
                        new KeyValuePair<double, double>(27.099601006841, 87.487516005325 ),
                        new KeyValuePair<double, double>(31.876509765884, 89.309330325801 ),
                        new KeyValuePair<double, double>(37.022659535515, 91.943520360815 ),
                        new KeyValuePair<double, double>(42.761358316331, 96.029367287105 ),
                        new KeyValuePair<double, double>(47.733831006693, 100.395517587082 ),
                        new KeyValuePair<double, double>(52.800737758743, 106.475089517329 ),
                        new KeyValuePair<double, double>(58.084492289287, 112.51526168998 ),
                        new KeyValuePair<double, double>(63.273290501029, 120.511754696542 ),
                        new KeyValuePair<double, double>(69.309117588517, 129.50681860565 ),
                        new KeyValuePair<double, double>(75.404419335909, 140.985212753492 ),
                    }),
                    polynomialConstant = 92.0724690025929,
                    polynomialLinear = -0.620441790150565,
                    polynomialQuadratic = 0.0167519308496172,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 60, r0 = 50, r1 = 70,
                    rawLengths = new List<double>(new double[] {
                        65.882280425745, 65.882280425746, 65.882280425746, 65.882280425746, 65.882280425746, 65.882280425746, 65.882280425746, 65.882280425746, 65.882280425746, 65.882280425746, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(0.723488397701, 55.908627073852 ),
                        new KeyValuePair<double, double>(4.204170907918, 52.928664561711 ),
                        new KeyValuePair<double, double>(6.513447347098, 51.458109521947 ),
                        new KeyValuePair<double, double>(9.892004497593, 49.234955140565 ),
                        new KeyValuePair<double, double>(14.198667878362, 47.985499916849 ),
                        new KeyValuePair<double, double>(18.3273662675, 47.715569186871 ),
                        new KeyValuePair<double, double>(21.665683565893, 48.241078987616 ),
                        new KeyValuePair<double, double>(24.375140410776, 49.151317910878 ),
                        new KeyValuePair<double, double>(28.634496627631, 51.782261776026 ),
                        new KeyValuePair<double, double>(33.070206983999, 55.712211805092 ),
                        new KeyValuePair<double, double>(37.038467589019, 60.097393176819 ),
                        new KeyValuePair<double, double>(39.50050133549, 62.630649677303 ),
                        new KeyValuePair<double, double>(44.26183867808, 70.473959958466 ),
                        new KeyValuePair<double, double>(48.703739235638, 77.368427834014 ),
                        new KeyValuePair<double, double>(53.297735402077, 86.469781196948 ),
                    }),
                    polynomialConstant = 56.6085969773783,
                    polynomialLinear = -1.02419753200131,
                    polynomialQuadratic = 0.0298855781868942,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 60, r0 = 70, r1 = 50,
                    rawLengths = new List<double>(new double[] {
                        65.880506105711, 65.880506105711, 65.880506105711, 65.880506105711, 65.880506105711, 65.880506105711, 65.880506105711, 65.880506105711, 65.880506105712, 65.880506105712, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(9.286274150632, 95.110757762135 ),
                        new KeyValuePair<double, double>(14.055606896831, 85.068906507503 ),
                        new KeyValuePair<double, double>(17.849017268773, 77.342293275161 ),
                        new KeyValuePair<double, double>(20.310111946486, 73.792169686986 ),
                        new KeyValuePair<double, double>(24.626902512848, 66.334778337349 ),
                        new KeyValuePair<double, double>(29.075952647173, 60.076202135824 ),
                        new KeyValuePair<double, double>(31.022598269137, 57.77553366791 ),
                        new KeyValuePair<double, double>(35.695329484296, 53.298602124653 ),
                        new KeyValuePair<double, double>(40.093249354128, 50.485576672369 ),
                        new KeyValuePair<double, double>(42.107783298304, 49.440569427127 ),
                        new KeyValuePair<double, double>(47.021852468808, 47.842159060975 ),
                        new KeyValuePair<double, double>(51.911421775669, 47.874229272807 ),
                        new KeyValuePair<double, double>(53.868006168439, 48.296497851349 ),
                        new KeyValuePair<double, double>(58.46187803744, 50.233043097538 ),
                        new KeyValuePair<double, double>(63.728655904836, 54.052378225686 ),
                    }),
                    polynomialConstant = 121.007511052144,
                    polynomialLinear = -2.96149351299958,
                    polynomialQuadratic = 0.0299740492250084,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 60, r0 = 70, r1 = 90,
                    rawLengths = new List<double>(new double[] {
                        86.042955492246, 86.042955492246, 86.042955492246, 86.042955492246, 86.042955492246, 86.042955492246, 86.042955492246, 86.042955492246, 86.042955492246, 86.042955492246, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(1.231467248016, 80.353129109542 ),
                        new KeyValuePair<double, double>(2.974165590865, 78.587938982991 ),
                        new KeyValuePair<double, double>(8.370240854033, 74.057951471814 ),
                        new KeyValuePair<double, double>(13.984892094459, 70.225880953107 ),
                        new KeyValuePair<double, double>(19.639024558794, 68.206389732929 ),
                        new KeyValuePair<double, double>(25.255753177439, 67.344957158507 ),
                        new KeyValuePair<double, double>(30.997653748632, 68.098179705916 ),
                        new KeyValuePair<double, double>(36.204150019037, 70.024967484533 ),
                        new KeyValuePair<double, double>(41.679885611652, 73.124710829033 ),
                        new KeyValuePair<double, double>(47.459713462439, 78.195445723006 ),
                        new KeyValuePair<double, double>(53.132099944117, 83.766583380362 ),
                        new KeyValuePair<double, double>(58.89676003052, 91.664893037797 ),
                        new KeyValuePair<double, double>(62.183345753346, 96.835645717893 ),
                        new KeyValuePair<double, double>(66.794398409972, 103.949235532899 ),
                        new KeyValuePair<double, double>(72.982528861955, 115.10915435736 ),
                    }),
                    polynomialConstant = 81.3635619287424,
                    polynomialLinear = -1.08007703409254,
                    polynomialQuadratic = 0.0212058008807247,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 60, r0 = 90, r1 = 70,
                    rawLengths = new List<double>(new double[] {
                        86.041132975104, 86.041132975104, 86.041132975104, 86.041132975104, 86.041132975104, 86.041132975104, 86.041132975104, 86.041132975104, 86.041132975104, 86.041132975104, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(5.271977864426, 133.344458280421 ),
                        new KeyValuePair<double, double>(12.07755911693, 118.513420291735 ),
                        new KeyValuePair<double, double>(17.751810121474, 108.532317563693 ),
                        new KeyValuePair<double, double>(20.845995934595, 102.439348756864 ),
                        new KeyValuePair<double, double>(25.914038709122, 94.159305159852 ),
                        new KeyValuePair<double, double>(31.384753962195, 86.947813653386 ),
                        new KeyValuePair<double, double>(36.853206206857, 80.823945455287 ),
                        new KeyValuePair<double, double>(43.012952111627, 75.092975081874 ),
                        new KeyValuePair<double, double>(48.924078854447, 70.920545073448 ),
                        new KeyValuePair<double, double>(52.196464180126, 69.244836722215 ),
                        new KeyValuePair<double, double>(58.177275662793, 67.575389243843 ),
                        new KeyValuePair<double, double>(63.522244947989, 67.427857917205 ),
                        new KeyValuePair<double, double>(69.014714776827, 68.754916307731 ),
                        new KeyValuePair<double, double>(75.348759964823, 71.684449713555 ),
                        new KeyValuePair<double, double>(81.801637305041, 76.969944323227 ),
                    }),
                    polynomialConstant = 147.674127283886,
                    polynomialLinear = -2.60865160184936,
                    polynomialQuadratic = 0.0212485409565421,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 80, r0 = 30, r1 = 50,
                    rawLengths = new List<double>(new double[] {
                        59.139983419375, 59.139983419375, 59.139983419375, 59.139983419376, 59.139983419376, 59.139983419377, 59.139983419377, 59.139983419377, 59.139983419378, 59.139983419378, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(1.585756556859, 41.628480157656 ),
                        new KeyValuePair<double, double>(4.579642622358, 37.335137995667 ),
                        new KeyValuePair<double, double>(7.247289475097, 33.877935419142 ),
                        new KeyValuePair<double, double>(9.579416473921, 31.731470610198 ),
                        new KeyValuePair<double, double>(12.564708771656, 29.847190367324 ),
                        new KeyValuePair<double, double>(15.822444891542, 28.863112187025 ),
                        new KeyValuePair<double, double>(18.527700859808, 28.7901524667 ),
                        new KeyValuePair<double, double>(20.912002462588, 29.387539302019 ),
                        new KeyValuePair<double, double>(23.278757750545, 30.56652070979 ),
                        new KeyValuePair<double, double>(25.933529882817, 32.315414284751 ),
                        new KeyValuePair<double, double>(29.011232668405, 35.992705050598 ),
                        new KeyValuePair<double, double>(31.578608661741, 39.125332347241 ),
                        new KeyValuePair<double, double>(33.804260242701, 42.780119155619 ),
                        new KeyValuePair<double, double>(36.678074322146, 48.075129455553 ),
                        new KeyValuePair<double, double>(39.661926106997, 54.134334045263 ),
                    }),
                    polynomialConstant = 44.0757345853967,
                    polynomialLinear = -1.75890086913732,
                    polynomialQuadratic = 0.0505732290412398,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 80, r0 = 50, r1 = 30,
                    rawLengths = new List<double>(new double[] {
                        59.137827754828, 59.137827754828, 59.137827754828, 59.137827754828, 59.137827754828, 59.137827754828, 59.137827754828, 59.137827754828, 59.137827754828, 59.137827754828, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(2.89985968603, 102.969211796497 ),
                        new KeyValuePair<double, double>(5.862118015622, 92.992282459415 ),
                        new KeyValuePair<double, double>(9.401328232333, 81.828244027311 ),
                        new KeyValuePair<double, double>(14.265560985173, 66.850348358389 ),
                        new KeyValuePair<double, double>(17.363476016933, 59.484370689105 ),
                        new KeyValuePair<double, double>(21.030962325584, 51.593649951769 ),
                        new KeyValuePair<double, double>(24.977563696845, 43.7517365024 ),
                        new KeyValuePair<double, double>(28.631241724148, 38.270537016769 ),
                        new KeyValuePair<double, double>(31.963390036851, 33.984708108705 ),
                        new KeyValuePair<double, double>(35.675163148193, 30.941877294871 ),
                        new KeyValuePair<double, double>(40.07748082025, 29.013302088416 ),
                        new KeyValuePair<double, double>(43.018304869092, 28.725177054127 ),
                        new KeyValuePair<double, double>(47.912161877355, 30.501831619452 ),
                        new KeyValuePair<double, double>(51.646915418557, 33.30094554614 ),
                        new KeyValuePair<double, double>(55.004089984485, 37.349776234785 ),
                    }),
                    polynomialConstant = 114.691000899098,
                    polynomialLinear = -4.02199319255255,
                    polynomialQuadratic = 0.0473163320327834,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 80, r0 = 50, r1 = 70,
                    rawLengths = new List<double>(new double[] {
                        85.834968382374, 85.834968382374, 85.834968382374, 85.834968382374, 85.834968382374, 85.834968382374, 85.834968382375, 85.834968382375, 85.834968382375, 85.834968382375, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(1.776504803251, 69.370512911244 ),
                        new KeyValuePair<double, double>(3.978655239612, 66.683125235974 ),
                        new KeyValuePair<double, double>(8.317025315458, 60.703314303204 ),
                        new KeyValuePair<double, double>(13.760829132636, 54.136042250255 ),
                        new KeyValuePair<double, double>(18.869686239836, 50.778811367965 ),
                        new KeyValuePair<double, double>(23.817780176631, 48.25509998498 ),
                        new KeyValuePair<double, double>(26.070514824442, 47.617346570963 ),
                        new KeyValuePair<double, double>(30.8625348866, 47.485749849422 ),
                        new KeyValuePair<double, double>(35.004739502706, 48.517943924871 ),
                        new KeyValuePair<double, double>(39.842171449557, 51.279624301689 ),
                        new KeyValuePair<double, double>(44.733156697051, 55.677435930889 ),
                        new KeyValuePair<double, double>(49.731968943425, 60.561360031592 ),
                        new KeyValuePair<double, double>(53.365566422246, 66.516851798832 ),
                        new KeyValuePair<double, double>(57.087535914707, 72.029393968655 ),
                        new KeyValuePair<double, double>(61.710265245826, 79.834859726083 ),
                    }),
                    polynomialConstant = 73.7465969313984,
                    polynomialLinear = -1.86890992439432,
                    polynomialQuadratic = 0.0320821197961421,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 80, r0 = 70, r1 = 50,
                    rawLengths = new List<double>(new double[] {
                        85.832707586219, 85.832707586219, 85.832707586219, 85.832707586219, 85.832707586219, 85.832707586219, 85.832707586219, 85.83270758622, 85.83270758622, 85.83270758622, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(3.575058529929, 132.84722319308 ),
                        new KeyValuePair<double, double>(9.670085682666, 115.749761796237 ),
                        new KeyValuePair<double, double>(15.379786513952, 102.448853675641 ),
                        new KeyValuePair<double, double>(20.545779009249, 90.448190977735 ),
                        new KeyValuePair<double, double>(25.815700688485, 78.159310287495 ),
                        new KeyValuePair<double, double>(30.590228851282, 70.558687102659 ),
                        new KeyValuePair<double, double>(36.336806649654, 61.637584067216 ),
                        new KeyValuePair<double, double>(42.098721975029, 54.81437349131 ),
                        new KeyValuePair<double, double>(47.090627028403, 51.268693675744 ),
                        new KeyValuePair<double, double>(52.588108608677, 48.212235112198 ),
                        new KeyValuePair<double, double>(57.694470605307, 47.398458769055 ),
                        new KeyValuePair<double, double>(63.407445458774, 48.261016102596 ),
                        new KeyValuePair<double, double>(69.329901256123, 51.372158788304 ),
                        new KeyValuePair<double, double>(74.423662044349, 55.809719411218 ),
                        new KeyValuePair<double, double>(80.303239237775, 62.992488137796 ),
                    }),
                    polynomialConstant = 146.710066269882,
                    polynomialLinear = -3.41419638899358,
                    polynomialQuadratic = 0.0294712845925872,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 90, r0 = 35, r1 = 55,
                    rawLengths = new List<double>(new double[] {
                        73.062075659429, 73.062075659429, 73.062075659429, 73.062075659429, 73.062075659429, 73.062075659429, 73.062075659429, 73.062075659429, 73.062075659429, 73.062075659429, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(0.894024118899, 56.150094669172 ),
                        new KeyValuePair<double, double>(5.404832503188, 48.297057073609 ),
                        new KeyValuePair<double, double>(8.243768981526, 43.914707824164 ),
                        new KeyValuePair<double, double>(12.973539144212, 38.670855700582 ),
                        new KeyValuePair<double, double>(16.431359263915, 35.565248932956 ),
                        new KeyValuePair<double, double>(21.120705116182, 33.406139851645 ),
                        new KeyValuePair<double, double>(24.982740138941, 32.985027194356 ),
                        new KeyValuePair<double, double>(28.003261151819, 33.675414703212 ),
                        new KeyValuePair<double, double>(32.152386443376, 36.155416609314 ),
                        new KeyValuePair<double, double>(36.085395021777, 39.515435636285 ),
                        new KeyValuePair<double, double>(39.9517592707, 44.02361561552 ),
                        new KeyValuePair<double, double>(42.88047787753, 48.428808316932 ),
                        new KeyValuePair<double, double>(46.968045238319, 56.313040999357 ),
                        new KeyValuePair<double, double>(50.67075169402, 63.994462291435 ),
                        new KeyValuePair<double, double>(55.332873552354, 74.510749563624 ),
                    }),
                    polynomialConstant = 58.225755305772,
                    polynomialLinear = -2.0748369308563,
                    polynomialQuadratic = 0.0430032078204315,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 90, r0 = 55, r1 = 35,
                    rawLengths = new List<double>(new double[] {
                        73.059669917776, 73.059669917776, 73.059669917776, 73.059669917776, 73.059669917776, 73.059669917776, 73.059669917776, 73.059669917776, 73.059669917776, 73.059669917776, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(4.862763697462, 116.230320641832 ),
                        new KeyValuePair<double, double>(7.817034881316, 106.420032544573 ),
                        new KeyValuePair<double, double>(13.359230929799, 88.401187924423 ),
                        new KeyValuePair<double, double>(18.044741899057, 76.344321672374 ),
                        new KeyValuePair<double, double>(22.461115216273, 65.616392137878 ),
                        new KeyValuePair<double, double>(25.22131122089, 59.226472396833 ),
                        new KeyValuePair<double, double>(30.660303519272, 49.605763080297 ),
                        new KeyValuePair<double, double>(35.654917074364, 42.068201901292 ),
                        new KeyValuePair<double, double>(39.996616551704, 37.345178493025 ),
                        new KeyValuePair<double, double>(42.875085858959, 35.128922254393 ),
                        new KeyValuePair<double, double>(48.536902587893, 32.983556096358 ),
                        new KeyValuePair<double, double>(53.789082697597, 33.651351518011 ),
                        new KeyValuePair<double, double>(58.253987748107, 36.104428424135 ),
                        new KeyValuePair<double, double>(61.243774941619, 38.686434270435 ),
                        new KeyValuePair<double, double>(67.115312241992, 46.0383724242 ),
                    }),
                    polynomialConstant = 136.371997957719,
                    polynomialLinear = -4.13460607016501,
                    polynomialQuadratic = 0.0415557046281334,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 90, r0 = 55, r1 = 75,
                    rawLengths = new List<double>(new double[] {
                        103.477430168939, 103.47743016894, 103.47743016894, 103.47743016894, 103.477430168941, 103.477430168941, 103.477430168941, 103.477430168941, 103.477430168941, 103.477430168941, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(4.469492795158, 82.97006472863 ),
                        new KeyValuePair<double, double>(6.113880527949, 81.078825969198 ),
                        new KeyValuePair<double, double>(12.15043668489, 70.877910949506 ),
                        new KeyValuePair<double, double>(18.144336892841, 62.705842117558 ),
                        new KeyValuePair<double, double>(24.101692028576, 56.646794250306 ),
                        new KeyValuePair<double, double>(29.951226599716, 53.269017950798 ),
                        new KeyValuePair<double, double>(35.779962253423, 51.255575688519 ),
                        new KeyValuePair<double, double>(41.610363036375, 51.50166525396 ),
                        new KeyValuePair<double, double>(47.415109805551, 53.430279028144 ),
                        new KeyValuePair<double, double>(53.146614002458, 57.782185169944 ),
                        new KeyValuePair<double, double>(58.569994523426, 63.074066786081 ),
                        new KeyValuePair<double, double>(64.099068191451, 69.824160494617 ),
                        new KeyValuePair<double, double>(69.747779680935, 79.798451648752 ),
                        new KeyValuePair<double, double>(75.434660351396, 89.598235499962 ),
                        new KeyValuePair<double, double>(77.375678924527, 93.889467665103 ),
                    }),
                    polynomialConstant = 92.7107748473884,
                    polynomialLinear = -2.16945803592566,
                    polynomialQuadratic = 0.0283774699463201,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                BendSwitchData data = new BendSwitchData() { angle = 90, r0 = 75, r1 = 55,
                    rawLengths = new List<double>(new double[] {
                        103.474950785831, 103.474950785831, 103.474950785831, 103.474950785831, 103.474950785831, 103.474950785831, 103.474950785831, 103.474950785831, 103.474950785831, 103.474950785831, 
                    }),
                    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {
                        new KeyValuePair<double, double>(4.41779887273, 153.881186091451 ),
                        new KeyValuePair<double, double>(11.162563581764, 135.270344986386 ),
                        new KeyValuePair<double, double>(17.772432996617, 115.634241162953 ),
                        new KeyValuePair<double, double>(24.250105023173, 100.647976360136 ),
                        new KeyValuePair<double, double>(30.637970981971, 87.486293850662 ),
                        new KeyValuePair<double, double>(37.007384896717, 76.218139943945 ),
                        new KeyValuePair<double, double>(43.448147397031, 66.925154825302 ),
                        new KeyValuePair<double, double>(49.958831511201, 58.702324675102 ),
                        new KeyValuePair<double, double>(56.538038806951, 53.989758092373 ),
                        new KeyValuePair<double, double>(63.184398820649, 51.299627288348 ),
                        new KeyValuePair<double, double>(69.896568497935, 51.438167095743 ),
                        new KeyValuePair<double, double>(76.670505970243, 54.448112712576 ),
                        new KeyValuePair<double, double>(83.50400150609, 59.4282199077 ),
                        new KeyValuePair<double, double>(90.399563995083, 67.953622530292 ),
                        new KeyValuePair<double, double>(97.355952098159, 79.27986185563 ),
                    }),
                    polynomialConstant = 171.931412131059,
                    polynomialLinear = -3.62595176475099,
                    polynomialQuadratic = 0.0273503842037196,
                    switchRadiisKnown = true,
                    processedRawRadiisCount = 15,
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 100, width = 20,
                    rawLengths = new List<double>(new double[] {
                        102.060274992931, 102.060274992932, 102.060274992933, 102.060274992933, 102.060274992933, 102.060274992934, 102.060274992934, 102.060274992934, 102.060274992934, 102.060274992934, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 102, width = 20,
                    rawLengths = new List<double>(new double[] {
                        103.961046181026, 104.021046181026, 104.021046181026, 104.021046181026, 104.021046181026, 104.021046181027, 104.021046181027, 104.021046181027, 104.021046181027, 104.021046181027, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 110, width = 20,
                    rawLengths = new List<double>(new double[] {
                        111.878033062569, 111.878033062569, 111.878033062569, 111.87803306257, 111.87803306257, 111.87803306257, 111.87803306257, 111.87803306257, 111.87803306257, 111.87803306257, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 70, width = 20,
                    rawLengths = new List<double>(new double[] {
                        72.874762247935, 72.904572777514, 72.904572777514, 72.904572777515, 72.904572777515, 72.904572777515, 72.904572777515, 72.904572777515, 72.904572777515, 72.904572777515, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 78, width = 20,
                    rawLengths = new List<double>(new double[] {
                        80.619013563811, 80.619013563811, 80.619013563812, 80.619013563812, 80.619013563812, 80.619013563812, 80.619013563812, 80.619013563812, 80.619013563812, 80.619013563812, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 90, width = 20,
                    rawLengths = new List<double>(new double[] {
                        92.281681272439, 92.281681272439, 92.281681272439, 92.281681272439, 92.281681272439, 92.281681272439, 92.281681272439, 92.281681272439, 92.281681272439, 92.281681272439, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 94, width = 20,
                    rawLengths = new List<double>(new double[] {
                        96.187657083814, 96.187657083815, 96.187657083815, 96.187657083815, 96.187657083815, 96.187657083815, 96.187657083815, 96.187657083815, 96.187657083815, 96.187657083816, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
            {
                StraightSwitchData data = new StraightSwitchData() { length = 99, width = 20,
                    rawLengths = new List<double>(new double[] {
                        101.080466146794, 101.080466146798, 101.080466146798, 101.080466146799, 101.0804661468, 101.0804661468, 101.0804661468, 101.0804661468, 101.0804661468, 101.0804661468, 
                    }),
                };
                data.ProcessLengthData();
                handler.AddData(data);
            }
        }
    }
}
