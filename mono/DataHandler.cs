using System;
using System.Collections.Generic;
using MathNet.Numerics;

namespace Bot {

	public class SwitchData {
		public bool switchLengthKnown = false;
		public double switchLength = 0.0;

		public List<double> rawLengths = new List<double>();

		public void ProcessLengthData() {
			if (rawLengths.Count == 0) return;

            if (rawLengths.Count > 50) {
                rawLengths.RemoveRange(0, rawLengths.Count - 50);
            }

			rawLengths.Sort();
			switchLengthKnown = true;
			switchLength = rawLengths[rawLengths.Count / 2];
		}
	}

	public class BendSwitchData : SwitchData {
		public int angle;
		public int r0;
		public int r1;

        public double polynomialConstant = 0.0;
        public double polynomialLinear = 0.0;
        public double polynomialQuadratic = 0.0;

		public bool switchRadiisKnown = false;
		//public double[] switchRadiis = new double[100];

		public List<KeyValuePair<double, double>> rawRadiis = new List<KeyValuePair<double, double>>();
        public int processedRawRadiisCount = 0;

		//public int GetRadiiBucketForDistance(double distance) {
		//	return Math.Max(0, Math.Min(switchRadiis.Length - 1, (int)((distance / switchLength) * 100)));
		//}

        public double GetSwitchRadii(double distance) {
            return polynomialConstant + polynomialLinear * distance + polynomialQuadratic * distance * distance;
        }

		public void ProcessRadiiData() {
            if (rawRadiis.Count < 5) return;

            //var sw = new System.Diagnostics.Stopwatch();
            //sw.Start();

            if (rawRadiis.Count > 25) {
                rawRadiis.RemoveRange(0, rawRadiis.Count - 25);
            }

            rawRadiis.Sort((KeyValuePair<double, double> p1, KeyValuePair<double, double> p2) => {
                return p1.Key.CompareTo(p2.Key);
            });

			/*List<double> xs = new List<double>();
			List<double> ys = new List<double>();

			int bucketSize = Math.Max(1, rawRadiis.Count / 10);
			int i = 0;
			while (i < rawRadiis.Count) {
				List<KeyValuePair<double, double>> foos = new List<KeyValuePair<double, double>>();

				for (int j=0; j < bucketSize && i < rawRadiis.Count; ++j, ++i) {
					foos.Add(rawRadiis[i]);
				}

				if (foos.Count == bucketSize) {
					foos.Sort((KeyValuePair<double, double> p1, KeyValuePair<double, double> p2) => {
						return p1.Value.CompareTo(p2.Value);
					});

					xs.Add(foos[foos.Count / 2].Key);
					ys.Add(foos[foos.Count / 2].Value);
				}
			}*/

            int count = rawRadiis.Count;
            double[] xs = new double[count];
            double[] ys = new double[count];

            for (int i=0; i<count; ++i) {
                xs[i] = rawRadiis[i].Key;
                ys[i] = rawRadiis[i].Value;
            }

			/*string str = "";
			foreach (double x in xs) str += string.Format("{0:0.00}, ", x);
			Console.WriteLine("{0} -> {1} - {2}", rawRadiis.Count, xs.Count, str);*/

			Control.DisableParallelization = true;
            double[] cs = Fit.Polynomial(xs, ys, 2);

            polynomialConstant = cs[0];
            polynomialLinear = cs[1];
            polynomialQuadratic = cs[2];

            //Console.WriteLine("Fitted {0}_{1}_{2}: A: {3:0.0000}, B: {4:0.0000}, C: {5:0.0000}", angle, r0, r1, polynomialConstant, polynomialLinear, polynomialQuadratic);

            //sw.Stop();
            //Console.WriteLine("Fit samples: {0}, time: {1} ms", rawRadiis.Count, sw.ElapsedMilliseconds);

            switchRadiisKnown = true;
            processedRawRadiisCount = rawRadiis.Count;

			/*List<double>[] radiis = new List<double>[switchRadiis.Length];
			for (int i=0; i<radiis.Length; ++i) {
				radiis[i] = new List<double>();
			}

			foreach (var pair in rawRadiis) {
				double distance = pair.Key;
				double radii = pair.Value;
				radiis[GetRadiiBucketForDistance(distance)].Add(radii);
			}

			int knownBucketCount = 0;

			for (int i=0; i<radiis.Length; ++i) {
				if (radiis[i].Count == 0) continue;
				knownBucketCount++;
				radiis[i].Sort();
				switchRadiis[i] = radiis[i][radiis[i].Count / 2];
			}

			if (knownBucketCount >= 5) switchRadiisKnown = true;*/
		}
	}

	public class StraightSwitchData : SwitchData {
		public int length;
		public int width;
	}

	public class DataHandler {

		List<BendSwitchData> bendSwitchDatas = new List<BendSwitchData>();
		List<StraightSwitchData> straightSwitchDatas = new List<StraightSwitchData>();

		public DataHandler () {
		}

		public void AddData(BendSwitchData data) {
			bendSwitchDatas.Add(data);
		}

		public void AddData(StraightSwitchData data) {
			straightSwitchDatas.Add(data);
		}

		public BendSwitchData GetBendSwitchData(double angle, double r0, double r1, bool createIfNecessary = false) {
			return GetBendSwitchData((int)Math.Abs(angle), (int)r0, (int)r1, createIfNecessary);
		}

		public BendSwitchData GetBendSwitchData(int angle, int r0, int r1, bool createIfNecessary = false) {
			//BendSwitchData bestMatch = null;

			foreach (BendSwitchData data in bendSwitchDatas) {
				if (data.angle == angle && data.r0 == r0 && data.r1 == r1) {
					return data;
				}
			}
			
			if (createIfNecessary) {
				BendSwitchData data = new BendSwitchData() { angle = angle, r0 = r0, r1 = r1 };
				bendSwitchDatas.Add(data);
				return data;
			}

			return null;
		}

		public StraightSwitchData GetStraightSwitchData(double length, double width, bool createIfNecessary = false) {
			return GetStraightSwitchData((int)length, (int)width, createIfNecessary);
		}

		public StraightSwitchData GetStraightSwitchData(int length, int width, bool createIfNecessary = false) {
			foreach (StraightSwitchData data in straightSwitchDatas) {
				if (data.length == length && data.width == width) {
					return data;
				}
			}

			if (createIfNecessary) {
				StraightSwitchData data = new StraightSwitchData() { length = length, width = width };
				straightSwitchDatas.Add(data);
				return data;
			}

			return null;
		}
	}
}

