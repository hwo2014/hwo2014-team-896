﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Bot {
    public class BendSwitchCalculator {

        public double angle;
        public double r0;
        public double r1;

        public double switchLength;
        public double[] switchRadiis;

        public Vector2[] points;

        public void Process() {
            points = new Vector2[101];

            for (int i = 0; i < points.Length; ++i) {
                double t = i / (double)(points.Length - 1);
                points[i] = EvaluatePoint(t);
            }

            /*switchLength = 0.0;
            for (int i = 1; i < points.Length; ++i) {
                double d = (points[i] - points[i - 1]).Length();
                //Console.WriteLine(d);
                switchLength += d;
            }*/

            switchLength = (points[1] - points[0]).Length() * 0.5;
            switchLength += (points[points.Length - 1] - points[points.Length - 2]).Length() * 0.5;

            switchRadiis = new double[points.Length - 1];
            for (int i = 2; i < points.Length; ++i) {
                Vector2 p0 = points[i - 2];
                Vector2 p1 = points[i - 1];
                Vector2 p2 = points[i];

                /*var matrixA = DenseMatrix.OfArray(new[,] { 
                    { 2 * (p0.x - p1.x), 2 * (p0.y - p1.y) }, 
                    { 2 * (p0.x - p2.x), 2 * (p0.y - p2.y) }, 
                });

                var vectorB = new DenseVector(new[] { 
                    p0.x * p0.x - p1.x * p1.x + p0.y * p0.y - p1.y * p1.y,
                    p0.x * p0.x - p2.x * p2.x + p0.y * p0.y - p2.y * p2.y,
                });

                var resultX = matrixA.LU().Solve(vectorB);

                Vector2 center = new Vector2(resultX[0], resultX[1]);*/

                Vector2 c0 = Vector2.Lerp(p0, p1, 0.5);
                Vector2 t0 = p1 - p0;

                Vector2 c1 = Vector2.Lerp(p1, p2, 0.5);
                Vector2 t1 = p2 - p1;

                Vector2 n0 = new Vector2(-t0.y, t0.x);
                Vector2 n1 = new Vector2(-t1.y, t1.x);

                Vector2 center;
                if (Vector2.LineIntersection(c0, c0 + n0, c1, c1 + n1, out center)) {

                    double r = (p1 - center).Length();

                    switchRadiis[i - 1] = r;

                    switchLength += r * (Math.Abs(Vector2.AngleBetween(n0, n1)) * DegreesMath.Deg2Rad);
                }

                //switchRadiis[i - 1] = (p1.Length() - r0) / (r1 - r0);
            }
        }

        Vector2 EvaluatePoint(double t) {

            //0,5 - (((0,5 -B10)+(0,5 -B10)^2))/(0,5 + 0,5^2)/2

            /*double q = -0.0;

            double rt;

            if (t < 0.5) {
                double s = 0.5 - t;
                rt = 0.5 - (s + s * s * q) / (0.5 + 0.5 * 0.5 * q) / 2;
            }
            else {
                double s = t - 0.5;
                rt = 0.5 + (s + s * s * q) / (0.5 + 0.5 * 0.5 * q) / 2;
            }*/

            //double rt = 1 - (1-t) * (1-t);

            Vector2 p = Vector2.FromAngle(t * angle) * MathUtils.Lerp(r0, r1, t);
            return p;
        }

    }
}
