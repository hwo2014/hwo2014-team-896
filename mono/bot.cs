using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Bot {
	public interface IBotVisualizer {
		void OnBotTrackUpdate(Bot bot, Track track);
		void OnBotCarsUpdate(Bot bot, CarPosition[] carPositions);
        void OnBotSimulationUpdate(Bot bot, CarState result);
		void OnBotTurboUpdate(Bot bot, Track.Piece turboStartPiece, Track.Piece turboEndPiece);
	}

	public interface ILogger {
		void Log(object obj);
	}

	public class NullLogger : ILogger {
		public void Log(object obj) {
		}
	}

	public class Bot : ILogger {
		public static void Main(string[] args) {
		    string host = args[0];
	        int port = int.Parse(args[1]);
	        string botName = args[2];
	        string botKey = args[3];

            Bot bot = new Bot();

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			using(TcpClient client = new TcpClient(host, port)) {
				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;

				bot.SetStreams(reader, writer);
				bot.Join(botName, botKey);
				//bot.CreateRace(botName, botKey, "keimola", "suchspeedy", 1);
				bot.Run();
			}
		}

		public bool enableLogging = true;
		public bool writeData = false;
		public bool stop = false;
		public int freezeTick = -1;
		public int startDelayTicks = 0;

		private StreamReader reader;
		private StreamWriter writer;

		public List<IBotVisualizer> visualizers = new List<IBotVisualizer>();

		public enum Algorithm {
			SwitchDataGathering, 
			Good, 
            Crashy, 
            Greedy
		}

		public Algorithm algorithm = Algorithm.Good;
		public double botSpeed = 1.0;

        class Car {
			public CarId carId;
            public CarPosition position;
			public Analyzer analyzer;
            public bool crashed = false;
            public int crashTick = 0;
            public bool finished = false;
            public int gameTick = 0;
            public CarStats stats = null;
        }

		List<Car> fillerCars = new List<Car>();
		public int fillerCarCount = 0;

		Race race = null;
		CarId myCarId = null;

        Dictionary<string, Car> cars = new Dictionary<string, Car>();
        Dictionary<string, CarStats> carStats = new Dictionary<string, CarStats>();

		NullLogger nullLogger = new NullLogger();
		PhysicsParams physicsParams = new PhysicsParams();

        ILogger verboseLogger;
        ILogger analyzerLogger;

		double carLength = 40.0;
		double currentThrottle = 1.0;
        double sentThrottle = 0.0;
        double turboAdjustedThrottle = 1.0;

        int responsesSent = 0;
        int currentGameTick = 0;
		int laneSwitchTick = -100;
        TurboAvailable turboAvailable = null;
        TurboAvailable activatedTurbo = null;
        Track.Piece prevTurboAvailablePiece = null;
        bool turboActive = false;
		int prevSwitchDirection = 0;
        int respawnTicks = 400;

        int ticksUntilCrashyCrash = 0;
        public int crashyCrashInterval = 0;

        DataHandler dataHandler = new DataHandler();

        public Bot() {
            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            Data.FillData(dataHandler);

            stopwatch.Stop();
            Log(string.Format("Data prepare time: {0} ms", stopwatch.ElapsedMilliseconds));
		}

		public void SetStreams(StreamReader reader, StreamWriter writer) {
			this.reader = reader;
			this.writer = writer;
		}

		public void Join(string botName, string botKey) {
			send(new Join(botName, botKey));
		}

		public void CreateRace(string botName, string botKey, string trackName, string password, int carCount) {
			send(new CreateRace() { botId = new CreateRace.BotId() { name = botName, key = botKey }, trackName = trackName, password = password, carCount = carCount });
		}

		public void JoinRace(string botName, string botKey, string trackName, string password, int carCount) {
			send(new JoinRace() { botId = new JoinRace.BotId() { name = botName, key = botKey }, trackName = trackName, password = password, carCount = carCount });
		}

		public void Log(object obj) {
			if (enableLogging) {
				Console.WriteLine(obj.ToString());
			}
		}

		public void Run() {
            if (algorithm == Algorithm.Crashy) {
                ticksUntilCrashyCrash = crashyCrashInterval;
            }

            verboseLogger = nullLogger;
            analyzerLogger = this;

			string line;

			while(!stop && (line = reader.ReadLine()) != null) {
				Wrapper message = JsonConvert.DeserializeObject<Wrapper>(line);
                //Log(message.msgType);
                switch (message.msgType) {
					case "yourCar":
						HandleYourCar(message);
						break;
						
					case "tournamentEnd":
						HandleTournamentEnd(message);
						break;

					case "gameInit":
						HandleGameInit(message);
						break;

					case "carPositions":
						HandleCarPositions(message);
						break;

                    case "crash":
                        HandleCrash(message);
                        break;

                    case "spawn":
                        HandleSpawn(message);
                        break;

                    case "lapFinished":
                        HandleLapFinished(message);
                        break;

                    case "dnf":
                        HandleDidNotFinish(message);
                        break;

                    case "finish":
                        HandleFinish(message);
                        break;

                    case "turboAvailable":
                        HandleTurboAvailable(message);
                        break;

                    case "turboStart":
                        HandleTurboStart(message);
                        break;

                    case "turboEnd":
                        HandleTurboEnd(message);
                        break;
				}

                if (message.gameTickStr != null) {
                    currentGameTick = message.gameTick;
                }

				if (message.gameTickStr != null && message.gameTick < startDelayTicks) {
					if (responsesSent == message.gameTick) {
						send(new Throttle(message.gameTick, 0));

						writer.Flush();
						++responsesSent;
					}
				}
				else if (message.gameTickStr != null && (freezeTick < 0 || message.gameTick < freezeTick)) {

					SimulateFillerCars();

                    bool startTurbo = false;
					int laneSwitchDirection = 0;
					Track.Piece turboStartPiece = null;
					Track.Piece turboEndPiece = null;

                    Car myCar = null;;
                    if (myCarId != null) cars.TryGetValue(myCarId.color, out myCar);

					if (myCar != null && !myCar.crashed) {
						if (myCar.analyzer != null && myCar.analyzer.physicsParams.powerKnown && myCar.analyzer.physicsParams.dragKnown) {

                            if (botSpeed < 1.0) {
                                physicsParams.maxSlipAngle = 55 * botSpeed;
                            }

							/*
                        	//currentThrottle = analyzer.GetThrottleForTargetSpeed(5.935);
    	                    currentThrottle = analyzer.physicsParams.GetThrottleForTargetSpeed(analyzer.GetSample(0).speed, 5.5);

        	                if (Math.Abs(race.track.pieces[analyzer.GetSample(0).position.piecePosition.pieceIndex].angle) > 0.001 && 
							    race.track.pieces[analyzer.GetSample(0).position.piecePosition.pieceIndex].radius < 70) {
                	            //currentThrottle = 0.0;
                    	    }*/

							System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
							stopwatch.Start();

							CarState myCarState = CarStateForCar(myCar);

							Simulator simulator = new Simulator() { logger = this }; 
							simulator.track = race.track;
							simulator.physicsParams = physicsParams;
							simulator.turboFactor = activatedTurbo != null && turboActive ? activatedTurbo.turboFactor : 1.0;
							simulator.carLength = carLength;
							simulator.startGameTick = myCarState.gameTick;

							simulator.Init();

							Car[] otherCars = GetOtherCars();
							foreach (Car car in otherCars) {
								if (!car.finished && car.position.piecePosition.IsInFrontOfPosition(race.track, myCar.position.piecePosition)) {
									CarState state = CarStateForCar(car);
									//state.speed *= 0.9;
									simulator.AddOtherCar(state, car.stats, 200);
								}
							}

                            TrackRoute route = null;

							CarState result;
							if (algorithm == Algorithm.Good || algorithm == Algorithm.Crashy || algorithm == Algorithm.Greedy) {
                                Track.Piece nextSwitch = simulator.GetNextSwitchPiece(myCarState.piece);
                                Track.Piece nextSwitchAfterNext = nextSwitch != null ? simulator.GetNextSwitchPiece(nextSwitch) : null;

                                int maxChoices = Math.Min(3, race.track.lanes.Length);
                                int latestBumpTick = 0;
                                bool bestLaneWillCrash = true;

                                for (int choice = 0; choice < maxChoices; ++choice) {
                                    TrackRoute tmpRoute = simulator.GetShortestRoute(simulator.track.GetNextPiece(myCarState.piece).pieceIndex, myCarState.endLaneIndex, 500.0, choice);

                                    int bumpTick;
                                    if (simulator.WillBumpOtherCar(myCarState, myCar.stats, tmpRoute, nextSwitchAfterNext, out bumpTick)) {
                                    }
                                    else {
                                        bumpTick = int.MaxValue;
                                    }

                                    bool crash = simulator.WillCrash(myCarState, tmpRoute, myCarState.gameTick + 150);

                                    if (!bestLaneWillCrash && crash) continue;

                                    if (bumpTick > latestBumpTick || (bestLaneWillCrash && !crash)) {
                                        latestBumpTick = bumpTick;
                                        bestLaneWillCrash = crash;
                                        route = tmpRoute;
                                    }
                                }

                                route.pieces[myCarState.piece.pieceIndex].startLaneIndex = myCarState.startLaneIndex;
                                route.pieces[myCarState.piece.pieceIndex].endLaneIndex = myCarState.endLaneIndex;

                                if (latestBumpTick < int.MaxValue) {
                                    Log("Will bump in " + (latestBumpTick - myCarState.gameTick) + " ticks");
                                }

                                if (algorithm == Algorithm.Greedy) {
                                    result = simulator.GetMaxThrottle(myCarState, route, 50, 2, myCarState.gameTick + 150);
                                }
                                else {
                                    //result = simulator.SuchSimulation(myCarState, 0, myCarState.gameTick + 150);
                                    //result = simulator.GetOptimalThrottleV1(myCarState, route, 50, 2, myCarState.gameTick + 150);
                                    result = simulator.GetOptimalThrottleV2(myCarState, route, 50, 2, myCarState.gameTick + 150);
                                }
                            }
                            else {
								result = simulator.SimulateSwitchDataGathering(myCarState, 0, 100, prevSwitchDirection);
							}

							if (result == null) {
								currentThrottle = 0.0;
                                //Log("Will crash!");
							} else {
								foreach (IBotVisualizer visualizer in visualizers) {
									visualizer.OnBotSimulationUpdate(this, result);
								}

                                CarState[] resultStates = result.ToArray();

                                if (result.crashed && result.CrashedBeforeBend()) {
                                    currentThrottle = 1.0;
                                }
                                else {
                                    if (resultStates.Length >= 2) {
                                        currentThrottle = resultStates[1].throttle;
                                    }
                                }

								for (int i=1; i<Math.Min(resultStates.Length, 4); ++i) {
									if (resultStates[i - 1].canSwitchLane && resultStates[i].startLaneIndex != resultStates[i].endLaneIndex) {
										laneSwitchDirection = resultStates[i].startLaneIndex < resultStates[i].endLaneIndex ? 1 : -1;
										break;
									}
								}

								/*int bumpTick = -1;
								for (int i=0; i<resultStates.Length; ++i) {
									if (resultStates[i].bumpedCar) {
										bumpTick = i;
										break;
									}
								}
								Log("Bumped on tick " + bumpTick);*/
							}

							if (!turboActive) {
								PhysicsParams.TrackParams.Piece pieceParams = physicsParams.GetTrackParams(race.track).pieces[myCar.position.piecePosition.pieceIndex];
								pieceParams.throttleSum += currentThrottle;
								pieceParams.throttleCount++;
							}

							stopwatch.Stop();
							Log(string.Format("Simulation time: {0} ms", stopwatch.ElapsedMilliseconds));


                            if (simulator.FindOptimalTurboStartPieceV2(prevTurboAvailablePiece != null ? prevTurboAvailablePiece : race.track.pieces[0], 
                                turboAvailable != null ? turboAvailable.turboDurationTicks : 30, out turboStartPiece, out turboEndPiece)) {

								foreach (IBotVisualizer visualizer in visualizers) {
									visualizer.OnBotTurboUpdate(this, turboStartPiece, turboEndPiece);
								}
							}

                            //turboStartPiece != null && myCar.position.piecePosition.pieceIndex == turboStartPiece.pieceIndex)

                            if (simulator.CanDriveAtThrottleToPiece(myCarState, route, 1.5, turboStartPiece, myCarState.gameTick + 30, myCarState.gameTick + 180)) {
                                startTurbo = true;
                            }

                            if (algorithm == Algorithm.Crashy) {
                                --ticksUntilCrashyCrash;
                                if (ticksUntilCrashyCrash <= 0) currentThrottle = 1.0;
                            }

							//currentThrottle = 0.6;

							//double radius = myCar.analyzer.GetSample(1).position.piecePosition.GetBendRadius(race.track);

							/*if (radius > 0) {
								double speed = myCar.analyzer.GetSample(1).speed;
								double force = speed * speed / radius;

								Log(string.Format("Speed: {0:0.0000}, radius: {1:0.0000}, drift: {2:0.0000}, force: {3:0.0000}", 
								                  speed, 
								                  radius, 
								                  myCar.analyzer.GetSample(0).position.angle, 
								                  force));
							}*/
                        }
                    }

                    turboAdjustedThrottle = currentThrottle * (activatedTurbo != null && turboActive ? activatedTurbo.turboFactor : 1.0);

                    verboseLogger.Log(string.Format("Tick: {0}, Sent: {1}", message.gameTick, responsesSent));

                    if (responsesSent == message.gameTick) {

                        //currentThrottle = myCar.analyzer.physicsParams.GetThrottleForTargetSpeed(myCar.analyzer.currentSpeed, 5.8);
                        //turboAdjustedThrottle = currentThrottle * (activatedTurbo != null && turboActive ? activatedTurbo.turboFactor : 1.0);


                        /*if (myCar != null && myCar.position.piecePosition.lap * race.track.pieces.Length + myCar.position.piecePosition.pieceIndex >= 115) {
                            currentThrottle = myCar.analyzer.physicsParams.GetThrottleForTargetSpeed(myCar.analyzer.currentSpeed, 7);
                            turboAdjustedThrottle = currentThrottle * (activatedTurbo != null && turboActive ? activatedTurbo.turboFactor : 1.0);

                            if (myCar.analyzer.physicsParams.minSlipForceKnown && race.track.pieces[myCar.position.piecePosition.pieceIndex].isBend) {
                                Log(string.Format("Piece: {0}, Radius: {1:0.0}, Speed: {2:0.00}, Force: {3:0.000}, Min slip force: {4:0.000}",
                                    myCar.position.piecePosition.pieceIndex,
                                    myCar.position.piecePosition.GetBendRadius(race.track),
                                    myCar.analyzer.currentSpeed,
                                    Math.Abs(myCar.analyzer.currentCentrifugalForce),
                                    myCar.analyzer.physicsParams.minSlipForce));
                            }
                        }*/

						if (turboAvailable != null && !turboActive && (myCar != null && !myCar.crashed && startTurbo)) {
                            activatedTurbo = turboAvailable;
                            turboAvailable = null;
                            send(new Turbo(message.gameTick, "Such speed, WOW!"));
                            Log("Activating turbo!");
                        }
                        else if (laneSwitchDirection != 0 && message.gameTick > laneSwitchTick + 10) {
							laneSwitchTick = message.gameTick;
							prevSwitchDirection = laneSwitchDirection;
							string direction = laneSwitchDirection < 0 ? "Left" : "Right";
                            send(new SwitchLane(message.gameTick, direction));
							//Log("Switching lane to " + direction);
                        }
                        else {
                            send(new Throttle(message.gameTick, currentThrottle));

                            if (currentThrottle != sentThrottle) {
                                sentThrottle = currentThrottle;
                                //Log(string.Format("Throttle: {0:0.000}{1}, Min slip force: {2:0.000}", currentThrottle, turboActive ? " (Turbo!)" : "", physicsParams.minSlipForce));
                            }

                            if (myCar != null) {

                                Log(string.Format("Throttle: {0:0.000}{1}, Speed: {2,6:0.000}, Angle: {3,6:0.00}, Max Angle: {4:0.00}, Power: {5:0.0000}, Drag: {6:0.0000}, Centrifugal: {7:0.000}, Angle Acceleration: {8:0.00000}",
                                    currentThrottle, turboActive ? " (Turbo!)" : "", myCar.analyzer.currentSpeed, Math.Abs(myCar.position.angle), myCar.analyzer.maxSlipAngle,
                                    physicsParams.power, physicsParams.drag, physicsParams.slipCentrifugalForceFactor, physicsParams.slipAngleAccelerationFactor));
                            }
                        }

                        writer.Flush();
                        ++responsesSent;
                    }
				}
			}

            Log("Bot exited normally");
		}

		void HandleYourCar(Wrapper message) {
			myCarId = message.jdata.ToObject<CarId>();
			Log("My car: " + JsonConvert.SerializeObject(myCarId));
		}

        void HandleCrash(Wrapper message) {
            CarId id = message.jdata.ToObject<CarId>();

            Car car;
            if (cars.TryGetValue(id.color, out car)) {
                car.crashed = true;
                car.crashTick = currentGameTick;

                if (myCarId != null && id.color == myCarId.color) {
                    Log("Crashed!");

                    //physicsParams.maxSlipAngle -= 1.5;
                    //Log("New max slip angle: " + physicsParams.maxSlipAngle);

                    turboActive = false;

                    Car myCar = null; ;
                    if (cars.TryGetValue(myCarId.color, out myCar)) {
                        myCar.analyzer.maxSlipAngle = 0.0;
                    }
                }
            }
        }

        void HandleSpawn(Wrapper message) {
            CarId id = message.jdata.ToObject<CarId>();

            Car car;
            if (cars.TryGetValue(id.color, out car)) {
                car.crashed = false;

                respawnTicks = currentGameTick - car.crashTick;

                if (myCarId != null && id.color == myCarId.color) {
                    Log("Spawned!");

                    if (algorithm == Algorithm.Crashy) {
                        ticksUntilCrashyCrash = crashyCrashInterval;
                    }
                }
            }
        }

		void HandleTournamentEnd(Wrapper message) {
			physicsParams = new PhysicsParams();
            carStats.Clear();
		}

		void HandleGameInit(Wrapper message) {
            cars.Clear();
            currentThrottle = 1.0;
            turboAdjustedThrottle = 1.0;
            responsesSent = 0;
			laneSwitchTick = -100;
            turboAvailable = null;
            activatedTurbo = null;
            turboActive = false;

			race = message.jdata["race"].ToObject<Race>();
			race.track.BuildTrack(dataHandler);
			//Log(JsonConvert.SerializeObject(race, Formatting.Indented));

            foreach (Track.Piece piece in race.track.pieces) {
                if (piece.isSwitch) {
                    Log(piece);
                }
            }

			if (race.cars != null && race.cars.Length > 0) {
				carLength = race.cars[0].dimensions.length;
			}

			foreach (IBotVisualizer visualizer in visualizers) {
				visualizer.OnBotTrackUpdate(this, race.track);
			}

			if (fillerCarCount > 0) {
				Random random = new Random();
				for (int i=0; i<fillerCarCount; ++i) {
					int lane = random.Next() % race.track.lanes.Length;

					Car car = new Car();
					car.carId = new CarId() { color = "c" + random.Next(), name = "n" + random.Next() };
					car.position = new CarPosition() { id = car.carId, angle = 0, piecePosition = new PiecePosition() {
							pieceIndex = random.Next() % race.track.pieces.Length, 
							lane = new PiecePosition.Lane() { 
								startLaneIndex = lane, 
								endLaneIndex = lane
							}
						}
					};
					car.analyzer = new Analyzer() { currentSpeed = MathUtils.Lerp(2, 5, random.NextDouble()) };

					fillerCars.Add(car);
				}
			}
		}

		void HandleCarPositions(Wrapper message) {

			List<CarPosition> carPositions = new List<CarPosition>();
            for (int i = 0; i < message.jarray.Count; ++i) {
				CarPosition carPosition = message.jarray[i].ToObject<CarPosition>();
				carPositions.Add(carPosition);

                Car car;
                if (!cars.TryGetValue(carPosition.id.color, out car)) {
					car = new Car() { carId = carPosition.id };
                    cars[carPosition.id.color] = car;

                    if (!carStats.TryGetValue(car.carId.color, out car.stats)) {
                        car.stats = new CarStats(race.track);
                        carStats[car.carId.color] = car.stats;
                    }

					car.analyzer = new Analyzer() { 
						logger = analyzerLogger, 
						track = race.track, 
						carId = carPosition.id, 
						physicsParams = physicsParams, 
						isMyCar = car.carId.color == myCarId.color, 
						writeData = writeData, 
                        carStats = car.stats,
					};
                }

                car.position = carPosition;
                car.gameTick = message.gameTick;

				if (!car.crashed && !car.finished && car.analyzer != null) {
	                car.analyzer.AddSample(message.gameTick, car.position, car.carId.color == myCarId.color ? turboAdjustedThrottle : 0.0);
                }
			}

			foreach (Car filler in fillerCars) {
				Car myCar = null;;
				if (myCarId != null) cars.TryGetValue(myCarId.color, out myCar);

				if (filler.position.piecePosition.IsInFrontOfPosition(race.track, myCar.position.piecePosition)) {
					carPositions.Add(filler.position);
				}
			}

			foreach (IBotVisualizer visualizer in visualizers) {
				visualizer.OnBotCarsUpdate(this, carPositions.ToArray());
			}
		}

        void HandleTurboAvailable(Wrapper message) {
            Car myCar = null; ;
            if (myCarId != null) cars.TryGetValue(myCarId.color, out myCar);

            if (myCar != null && !myCar.crashed) {
                prevTurboAvailablePiece = race.track.pieces[myCar.position.piecePosition.pieceIndex];
                turboAvailable = message.jdata.ToObject<TurboAvailable>();
                Log(string.Format("Turbo available, duration: {0} ticks, factor: {1:0.00}", turboAvailable.turboDurationTicks, turboAvailable.turboFactor));
            }
        }

        void HandleTurboStart(Wrapper message) {
            CarId carId = message.jdata.ToObject<CarId>();
            if (carId.color == myCarId.color) {
                turboActive = true;
                Log("Turbo start!");
            }
        }

        void HandleTurboEnd(Wrapper message) {
            CarId carId = message.jdata.ToObject<CarId>();
            if (carId.color == myCarId.color) {
                turboActive = false;
                Log("Turbo end");
            }
        }

        void HandleLapFinished(Wrapper message) {
            LapFinished lapFinished = message.jdata.ToObject<LapFinished>();

            int ms = lapFinished.lapTime.millis;
            int seconds = ms / 1000;
            ms -= seconds * 1000;

            string timeStr = string.Format("{0:00}.{1:00}", seconds, ms / 10);

            Log(string.Format("####################### {0} finished lap in {1} ({2} ticks) #######################", lapFinished.car, timeStr, lapFinished.lapTime.ticks));

            if (lapFinished.car.color == myCarId.color) {
                Car myCar = null; ;
                if (cars.TryGetValue(myCarId.color, out myCar)) {
                    myCar.analyzer.maxSlipAngle = 0.0;
                }
            }
        }

        void HandleDidNotFinish(Wrapper message) {
            DidNotFinish dnf = message.jdata.ToObject<DidNotFinish>();
            Car car;
            if (cars.TryGetValue(dnf.car.color, out car)) {
                car.finished = true;
                Log("####################### DNF: " + dnf.car + ", " + dnf.reason + " #######################");
            }
        }

        void HandleFinish(Wrapper message) {
            CarId id = message.jdata.ToObject<CarId>();
            Car car;
            if (cars.TryGetValue(id.color, out car)) {
                car.finished = true;
                Log("####################### Finished: " + id + " #######################");
            }
        }

		private void send(SendMsg msg) {
			//Log("Sending: " + msg.ToJson());
			writer.WriteLine(msg.ToJson());
		}

		CarState CarStateForCar(Car car) {
			CarState state = new CarState();
			state.prevState = null;
			state.gameTick = car.gameTick;
			state.piece = race.track.pieces[car.position.piecePosition.pieceIndex];
			state.inPieceDistance = car.position.piecePosition.inPieceDistance;
			state.lap = car.position.piecePosition.lap;
			state.speed = !car.crashed ? car.analyzer.currentSpeed : 0.0;
			state.throttle = currentThrottle;
			state.angle = car.position.angle;
			state.angularSpeed = car.analyzer.currentAngularSpeed;
			state.startLaneIndex = car.position.piecePosition.lane.startLaneIndex;
			state.endLaneIndex = car.position.piecePosition.lane.endLaneIndex;
			state.crashed = car.crashed;
            state.ticksUntilRespawn = car.crashed ? car.crashTick + respawnTicks - currentGameTick : 0;
			state.bumpedCar = false;

			state.canSwitchLane = false;
            state.startLaneOffset = race.track.lanes[state.startLaneIndex].distanceFromCenter;
            state.endLaneOffset = race.track.lanes[state.endLaneIndex].distanceFromCenter;
            state.pieceLength = state.startLaneIndex != state.endLaneIndex ? state.piece.GetLaneSwitchLength(state.startLaneIndex, state.endLaneIndex) : state.piece.GetLaneLength(state.startLaneOffset);

			return state;
		}

		void SimulateFillerCars() {
			if (race.track != null && fillerCars.Count > 0) {
				Simulator simulator = new Simulator() {
					logger = nullLogger, 
					track = race.track, 
					physicsParams = physicsParams, 
					carLength = carLength
				};
				simulator.Init();

				foreach (Car car in fillerCars) {
					CarState state = CarStateForCar(car);

					CarState newState = simulator.SimpleTick(state);

					car.gameTick = newState.gameTick;
					car.position.piecePosition.pieceIndex = newState.piece.pieceIndex;
					car.position.piecePosition.inPieceDistance = newState.inPieceDistance;
				}
			}
		}

		Car[] GetOtherCars() {
			List<Car> otherCars = new List<Car>();

			foreach (KeyValuePair<string, Car> pair in cars) {
				Car car = pair.Value;
				if (car.carId != myCarId) otherCars.Add(car);
			}

			foreach (Car car in fillerCars) {
				otherCars.Add(car);
			}

			return otherCars.ToArray();
		}

	}

	class MsgWrapper {
	    public string msgType;
	    public Object data;

	    public MsgWrapper(string msgType, Object data) {
	    	this.msgType = msgType;
	    	this.data = data;
	    }
	}

	abstract class SendMsg {
		public string ToJson() {
			return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
		}
		protected virtual Object MsgData() {
	        return this;
	    }

	    protected abstract string MsgType();
	}

	class Join : SendMsg {
		public string name;
		public string key;

		public Join(string name, string key) {
			this.name = name;
			this.key = key;
		}

		protected override string MsgType() { 
			return "join";
		}
	}

	class CreateRace : SendMsg {
		public class BotId {
			public string name;
			public string key;
		}

		public BotId botId;
		public string trackName;
		public string password;
		public int carCount;

		protected override string MsgType() { 
			return "createRace";
		}
	}
	
	class JoinRace : SendMsg {
		public class BotId {
			public string name;
			public string key;
		}

		public BotId botId;
		public string trackName;
		public string password;
		public int carCount;

		protected override string MsgType() { 
			return "joinRace";
		}
	}

	class Ping: SendMsg {
		protected override string MsgType() {
			return "ping";
		}
	}

    class Turbo : SendMsg {
        public int gameTick;
        public string message;

        public Turbo(int gameTick, string message) {
            this.gameTick = gameTick;
            this.message = message;
        }

        protected override Object MsgData() {
            return this.message;
        }

        protected override string MsgType() {
            return "turbo";
        }
    }

	class Throttle: SendMsg {
        public int gameTick;
		public double value;

		public Throttle(int gameTick, double value) {
            this.gameTick = gameTick;
			this.value = value;
		}

		protected override Object MsgData() {
			return this.value;
		}

		protected override string MsgType() {
			return "throttle";
		}
	}

    class SwitchLane : SendMsg {
        public int gameTick;
        public string direction;

        public SwitchLane(int gameTick, string direction) {
            this.gameTick = gameTick;
            this.direction = direction;
        }

        protected override object MsgData() {
            return direction;
        }

        protected override string MsgType() {
            return "switchLane";
        }
    }
}