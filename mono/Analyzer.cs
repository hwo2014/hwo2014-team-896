using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Bot {

    public class SampleQueue<T> {

        public int maxSamples = 60;
        public List<T> samples = new List<T>();

        public void AddSample(T sample) {
            samples.Add(sample);
            while (samples.Count > maxSamples) samples.RemoveAt(0);
        }

        public T GetMedian() {
            T[] sorted = samples.ToArray();
            Array.Sort(sorted);

            return sorted[sorted.Length / 2];
        }
    }

    public class CarStats {
        public class Segment {
            public int pieceIndex;
            public int segmentIndex;
            public SampleQueue<double> speed = new SampleQueue<double>() { maxSamples = 7 };
        }

        public class Piece {
            public Segment[] segments;
            public double segmentLength;
        }

        public Track track;
        public Piece[] pieces;

        public CarStats(Track track) {
            this.track = track;
            pieces = new Piece[track.pieces.Length];
            for (int pieceIndex = 0; pieceIndex < pieces.Length; ++pieceIndex) {
                double pieceLength = track.pieces[pieceIndex].GetLaneLength(0);
                int segmentCount = Math.Max(1, (int)Math.Ceiling(pieceLength / 20));

                pieces[pieceIndex] = new Piece();
                pieces[pieceIndex].segments = new Segment[segmentCount];
                pieces[pieceIndex].segmentLength = pieceLength / segmentCount;
                for (int i=0; i<segmentCount; ++i) {
                    pieces[pieceIndex].segments[i] = new Segment() { pieceIndex = pieceIndex, segmentIndex = i };
                }
            }
        }

        int GetSegmentIndex(PiecePosition piecePosition) {
            Piece piece = pieces[piecePosition.pieceIndex];

            double pieceLength = piecePosition.GetPieceLength(track);
            double t = piecePosition.inPieceDistance / pieceLength;
            return Math.Max(0, Math.Min(piece.segments.Length - 1, (int)(t * piece.segments.Length)));
        }

        Segment GetSegment(PiecePosition piecePosition) {
            return pieces[piecePosition.pieceIndex].segments[GetSegmentIndex(piecePosition)];
        }

        Segment GetNextSegment(Segment segment) {
            int pieceIndex = segment.pieceIndex;
            int segmentIndex = segment.segmentIndex;

            ++segmentIndex;
            if (segmentIndex >= pieces[pieceIndex].segments.Length) {
                segmentIndex = 0;
                ++pieceIndex;
                if (pieceIndex >= track.pieces.Length) pieceIndex = 0;
            }

            return pieces[pieceIndex].segments[segmentIndex];
        }

        Segment GetPrevSegment(Segment segment) {
            int pieceIndex = segment.pieceIndex;
            int segmentIndex = segment.segmentIndex;

            --segmentIndex;
            if (segmentIndex < 0) {
                --pieceIndex;
                if (pieceIndex < 0) pieceIndex = track.pieces.Length - 1;

                segmentIndex = pieces[pieceIndex].segments.Length - 1;
            }

            return pieces[pieceIndex].segments[segmentIndex];
        }

        Segment GetEarlierSegment(Segment segment, double maxDistance, out double distance, Func<Segment, bool> condition) {
            distance = pieces[segment.pieceIndex].segmentLength * 0.5;
            while (distance < maxDistance) {
                segment = GetPrevSegment(segment);
                if (condition(segment)) return segment;
                distance += pieces[segment.pieceIndex].segmentLength;
            }
            return null;
        }

        Segment GetLaterSegment(Segment segment, double maxDistance, out double distance, Func<Segment, bool> condition) {
            distance = pieces[segment.pieceIndex].segmentLength * 0.5;
            while (distance < maxDistance) {
                segment = GetNextSegment(segment);
                if (condition(segment)) return segment;
                distance += pieces[segment.pieceIndex].segmentLength;
            }
            return null;
        }

        public void AddSpeed(PiecePosition piecePosition, double speed) {
            Segment segment = GetSegment(piecePosition);
            segment.speed.AddSample(speed);
        }

        public bool TryGetSpeed(PiecePosition piecePosition, out double speed) {
            speed = 0.0;

            Segment segment = GetSegment(piecePosition);
            if (segment.speed.samples.Count > 0) {
                speed = segment.speed.GetMedian();
                return true;
            }
            else {
                double prevDistance, nextDistance;
                Segment prevSegment = GetEarlierSegment(segment, 50, out prevDistance, (Segment s) => { return s.speed.samples.Count > 0; });
                Segment nextSegment = GetLaterSegment(segment, 50, out nextDistance, (Segment s) => { return s.speed.samples.Count > 0; });

                if (prevSegment != null && nextSegment != null) {
                    double t = prevDistance / (prevDistance + nextDistance);
                    speed = MathUtils.Lerp(prevSegment.speed.GetMedian(), nextSegment.speed.GetMedian(), t);
                    return true;
                }
            }

            return false;
        }
    }

    public class PhysicsParams {
		public class TrackParams {
			public class Piece {
				public double throttleSum = 0.0;
				public int throttleCount = 0;

				public double averageThrottle {
					get { return throttleCount > 0 ? throttleSum / throttleCount : 0.0; }
				}
			}

			public Track track;
			public Piece[] pieces;
		}

        public bool powerKnown = false;
        public double power = 0.5;

        public bool dragKnown = false;
        public double drag = 0.05;

        public bool slipFactorsKnown = false;
        public double slipCentrifugalForceFactor = 0.53;
        public double slipAngleAccelerationFactor = 0.00125;

		TrackParams trackParams;
		public TrackParams GetTrackParams(Track track) {
			if (trackParams == null || trackParams.track != track) {
				trackParams = new TrackParams() { track = track };
				trackParams.pieces = new TrackParams.Piece[track.pieces.Length];
				for (int i=0; i<trackParams.pieces.Length; ++i) {
					trackParams.pieces[i] = new TrackParams.Piece();
				}
			}

			return trackParams;
		}

		public double maxSpeed = 0.0;

        // constants
        public double slipDownforceFactor = 240.0; 
        public double slipDampeningFactor = 0.1;
        public double maxSlipAngle = 55.0;

        public double GetThrottleForTargetSpeed(double currentSpeed, double targetSpeed, double turboFactor) {
            return MathUtils.Clamp01((targetSpeed - currentSpeed + currentSpeed * drag) / (power * turboFactor));
        }
    }

	public class Analyzer {
		public ILogger logger;
		public Track track;
		public CarId carId;
		public bool isMyCar;
		public bool writeData;

		public class Sample {
			public CarPosition position;
            public Track.Piece trackPiece;
            public double throttle;
			public double speed = 0.0;
            public double angularSpeed = 0.0;
			public bool turboActive = false;

			public double GetCentrifugalForce(Track track) {
				Track.Piece piece = track.pieces[position.piecePosition.pieceIndex];
				if (!piece.isBend) return 0;
				return speed * speed / position.piecePosition.GetBendRadius(track);
			}
		}

		List<Sample> samples = new List<Sample>();

        public PhysicsParams physicsParams = new PhysicsParams();
        public CarStats carStats = null;
        
        SampleQueue<double> powerSamples = new SampleQueue<double>();
        SampleQueue<double> dragSamples = new SampleQueue<double>();
        SampleQueue<double> centrifugalForceFactorSamples = new SampleQueue<double>();
        SampleQueue<double> angleAccelerationFactorSamples = new SampleQueue<double>();

        public double maxSlipAngle = 0.0;
		public double currentSpeed = 0.0;

		public double currentAngularSpeed { get { return samples.Count > 0 ? GetSample(0).angularSpeed : 0.0; } }
        public double currentCentrifugalForce = 0.0;

		public void AddSample(int gameTick, CarPosition position, double throttle) {
			Sample sample = new Sample() { position = position, trackPiece = track.pieces[position.piecePosition.pieceIndex], throttle = throttle };

            Sample prevSample = GetSample(0);
            bool samePieceAsPrevSample = false;

            if (prevSample != null) {
                if (sample.position.piecePosition.pieceIndex == prevSample.position.piecePosition.pieceIndex) {
                    sample.speed = sample.position.piecePosition.inPieceDistance - prevSample.position.piecePosition.inPieceDistance;
                    samePieceAsPrevSample = true;
                }
                else {
                    sample.speed = sample.position.piecePosition.inPieceDistance + prevSample.position.piecePosition.DistanceToPieceEnd(track);
                }

                sample.angularSpeed = sample.position.angle - prevSample.position.angle;
            }

			samples.Add(sample);

            if (carStats != null) {
                double estimatedSpeed;
                if (carStats.TryGetSpeed(sample.position.piecePosition, out estimatedSpeed)) {
                    //logger.Log(string.Format("Esimated speed: {0:0.00}, actual speed: {1:0.00}, diff: {2:0.00}", estimatedSpeed, sample.speed, sample.speed - estimatedSpeed));
                }

                carStats.AddSpeed(sample.position.piecePosition, sample.speed);
            }

            /*if (SpeedMatchesExpected()) {
                logger.Log("Speed matches");
            }
            else {
                logger.Log("Speed mismatch!");
            }*/

			currentSpeed = sample.speed;

            if (Math.Abs(sample.position.angle) > maxSlipAngle) maxSlipAngle = Math.Abs(sample.position.angle);

            if (samePieceAsPrevSample && sample.position.piecePosition.lane.startLaneIndex == sample.position.piecePosition.lane.endLaneIndex) {
				if (SpeedMatchesExpected() && sample.speed > physicsParams.maxSpeed) physicsParams.maxSpeed = sample.speed;
                AnalyzePowerAndDrag();
                if (!isMyCar || SpeedMatchesExpected()) AnalyzeGrip();
            }

            if (!isMyCar || SpeedMatchesExpected()) {
                AnalyzeSwitchBend();
            }

            AnalyzeSwitchLength();

            //logger.Log(string.Format("Speed: {0:0.0000}, Throttle: {1:0.0000}", sample.speed, sample.throttle));
            //logger.Log(string.Format("Angle: {0:0.0000}, Bend: {1:0.0}, Max Angle: {2:0.0000}", sample.position.angle, sample.position.piecePosition.GetBendRadius(track), maxSlipAngle));
        }

        bool SpeedMatchesExpected() {
            Sample sample0 = GetSample(1);
            Sample sample1 = GetSample(0);

            if (physicsParams.powerKnown && physicsParams.dragKnown) {
                double v1 = sample0.speed + sample1.throttle * physicsParams.power - sample0.speed * physicsParams.drag;

                //logger.Log(string.Format("Estimated speed: {0:0.0000}, observed speed: {1:0.0000}", v1, sample1.speed));

                if (Math.Abs(v1 - sample1.speed) > 0.001) {

                    return false;

                /*    logger.Log("############# Speed mismatch #############");
                    logger.Log(string.Format("Expected: {0}, observed: {1}, diff: {2}", v1, sample1.speed, v1 - sample1.speed));
                    logger.Log(sample0.position.piecePosition.ToString(track));
                    logger.Log(track.pieces[sample0.position.piecePosition.pieceIndex]);
                    logger.Log(sample1.position.piecePosition.ToString(track));
                    logger.Log(track.pieces[sample1.position.piecePosition.pieceIndex]);*/
                }
            }

            return true;
        }

		void AnalyzePowerAndDrag() {
			if (samples.Count < 3) return;

			double t0 = GetSample(1).throttle;
            double v0 = GetSample(2).speed;
			double dv0 = GetSample(1).speed - v0;
			double t1 = GetSample(0).throttle;
            double v1 = GetSample(1).speed;
			double dv1 = GetSample(0).speed - v1;

            if (t0 < 0.001) return;

            double denom = t1 / t0 * v0 - v1;
            if (Math.Abs(denom) < 0.001) return;

            double newDrag = (dv1 - t1 / t0 * dv0) / denom;
            double newPower = (dv0 + v0 * physicsParams.drag) / t0;

            if (newDrag > 0.0 && newDrag < 1.0 && newPower > 0.0) {
                dragSamples.AddSample(newDrag);
                powerSamples.AddSample(newPower);

                physicsParams.dragKnown = true;
                physicsParams.powerKnown = true;
                physicsParams.drag = dragSamples.GetMedian();
                physicsParams.power = powerSamples.GetMedian();

                //logger.Log(string.Format("Power: {0:0.0000}, Drag: {1:0.0000}", physicsParams.power, physicsParams.drag));
            }
		}

		string TabulatedString(double[] values) {
			string str = "";

			for (int i=0; i<values.Length; ++i) {
				if (i > 0) str += "\t";
				str += string.Format("{0:0.000000000000}", values[i]);
			}

			return str;
		}

		void PrintTabulated(double[] values) {
			logger.Log(TabulatedString(values));
		}

        void AnalyzeSwitchBend() {
            if (!physicsParams.slipFactorsKnown || samples.Count < 2) return;

            Sample sample0 = GetSample(1);
            Sample sample1 = GetSample(0);

            if (!sample0.trackPiece.isBend || !sample0.trackPiece.isSwitch) return;

			int startLane = sample0.position.piecePosition.lane.startLaneIndex;
			int endLane = sample0.position.piecePosition.lane.endLaneIndex;
			double inPieceDistance = sample0.position.piecePosition.inPieceDistance;

			if (sample0.position.piecePosition.lane.startLaneIndex == sample0.position.piecePosition.lane.endLaneIndex) return;

            double dw0 = sample1.angularSpeed - sample0.angularSpeed;
            double bendDirection = sample0.trackPiece.angle < 0 ? -1.0 : 1.0;

            double spring = -sample0.position.angle * sample0.speed * physicsParams.slipAngleAccelerationFactor;
            double dampening = -sample0.angularSpeed * physicsParams.slipDampeningFactor;

            double slipAcceleration = dw0 - spring - dampening;
            if (slipAcceleration * bendDirection < 0.001) return;

            double downforce = -physicsParams.slipDownforceFactor * physicsParams.slipAngleAccelerationFactor * sample0.speed;

            double centrifugal = slipAcceleration - downforce * bendDirection;

            double r = physicsParams.slipCentrifugalForceFactor * bendDirection * sample0.speed * sample0.speed / centrifugal;
            r *= r;

            if (r < 10 || r > 300) return;

            //logger.Log(string.Format("{0:0.000} {1:0.000}", sample0.position.piecePosition.GetBendRadius(track), r));

			double startRadius = sample0.position.piecePosition.GetStartLaneBendRadius(track);
			double endRadius = sample0.position.piecePosition.GetEndLaneBendRadius(track);


            double expectedRadii = sample0.trackPiece.GetLaneSwitchBendRadius(startLane, endLane, inPieceDistance);

            bool addedSample = false;
            BendSwitchData switchData = sample0.trackPiece.GetBendSwitchData(startLane, endLane);
            if (isMyCar || switchData.rawRadiis.Count < 10) {
                addedSample = true;
                switchData.rawRadiis.Add(new KeyValuePair<double, double>(inPieceDistance, r));
                if (switchData.rawRadiis.Count >= switchData.processedRawRadiisCount + 5) {
                    switchData.ProcessRadiiData();
                }
            }

            if (Math.Abs(expectedRadii - r) > 2.0) {
                logger.Log(string.Format("Unexpected switch radii, expected: {0:0.00000}, actual: {1:0.00000}, diff: {2:0.00000} (myCar: {3}, addedSample: {4})", expectedRadii, r, r - expectedRadii, isMyCar, addedSample));
            }
            else {
                logger.Log(string.Format("Switch radii {0:0.00000} matches expected (myCar: {1}, addedSample: {2})", r, isMyCar, addedSample));
            }

			if (isMyCar) {

				/*PrintTabulated(new double[] {
					sample0.position.piecePosition.pieceIndex, 
					Math.Abs(sample0.trackPiece.angle),
					startRadius, 
					endRadius, 
					sample0.speed, 
					centrifugal * bendDirection, 
					0.0,
					sample0.position.piecePosition.inPieceDistance, 
					r,
				});*/

				if (writeData) {
					string tag = string.Format("b_{0}_{1}_{2}_r.txt", (int)Math.Abs(sample0.trackPiece.angle), (int)startRadius, (int)endRadius);
					WriteData(tag, new double[] { sample0.position.piecePosition.inPieceDistance, r });
				}
			}
        }

        void AnalyzeSwitchLength() {
            if (samples.Count < 2) return;
			if (!isMyCar) return;

            Sample sample0 = GetSample(1);
            Sample sample1 = GetSample(0);

			int startLane = sample0.position.piecePosition.lane.startLaneIndex;
			int endLane = sample0.position.piecePosition.lane.endLaneIndex;

            if (sample0.trackPiece == sample1.trackPiece || sample0.position.piecePosition.lane.startLaneIndex == sample0.position.piecePosition.lane.endLaneIndex) return;

            if (!sample0.trackPiece.isSwitch) return;

			double speed1 = sample0.speed * (1.0 - physicsParams.drag) + sample1.throttle * physicsParams.power;
			double length = sample0.position.piecePosition.inPieceDistance + (speed1 - sample1.position.piecePosition.inPieceDistance);

            //logger.Log(sample0.trackPiece.GetLaneLength(track.lanes[sample0.position.piecePosition.lane.endLaneIndex].distanceFromCenter) + " " + length);

            double expectedLength = sample0.trackPiece.GetLaneSwitchLength(startLane, endLane);

            if (Math.Abs(expectedLength - length) > 0.001) {
                logger.Log(string.Format("Unexpected switch length, expected: {0:0.00000}, actual: {1:0.00000}, diff: {2:0.00000}", expectedLength, length, length - expectedLength));
            }
            else {
                logger.Log(string.Format("Switch length {0:0.00000} matches expected", length));
            }

			sample0.trackPiece.switchData[startLane][endLane].rawLengths.Add(length);
            sample0.trackPiece.switchData[startLane][endLane].ProcessLengthData();

			if (writeData) {
				string tag;

				if (sample0.trackPiece.isBend) {
					double startRadius = sample0.position.piecePosition.GetStartLaneBendRadius(track);
					double endRadius = sample0.position.piecePosition.GetEndLaneBendRadius(track);

					/*PrintTabulated(new double[] {
						sample0.position.piecePosition.pieceIndex, 
						Math.Abs(sample0.trackPiece.angle),
						startRadius, 
						endRadius, 
						0, 0, 
						length,
						0, 0, sample0.position.piecePosition.GetPieceLength(track)
					});*/

					tag = string.Format("b_{0}_{1}_{2}_l.txt", (int)Math.Abs(sample0.trackPiece.angle), (int)startRadius, (int)endRadius);
				} else {
					double startOffset = track.lanes[sample0.position.piecePosition.lane.startLaneIndex].distanceFromCenter;
					double endOffset = track.lanes[sample0.position.piecePosition.lane.endLaneIndex].distanceFromCenter;

					double distance = Math.Abs(endOffset - startOffset);

					tag = string.Format("s_{0}_{1}_l.txt", (int)Math.Abs(sample0.trackPiece.length), (int)distance);
				}

				WriteData(tag, new double[] { sample0.position.piecePosition.pieceIndex, length });
			}
        }

        void AnalyzeGrip() {
            if (samples.Count < 3) return;

            Sample sample0 = GetSample(2);
            Sample sample1 = GetSample(1);
            Sample sample2 = GetSample(0);

            if (!sample0.trackPiece.isBend || !sample1.trackPiece.isBend) return;
            if (sample0.position.piecePosition.IsSwitchingLane() || sample1.position.piecePosition.IsSwitchingLane() || sample2.position.piecePosition.IsSwitchingLane()) return;

            double dw0 = sample1.angularSpeed - sample0.angularSpeed;
            double dw1 = sample2.angularSpeed - sample1.angularSpeed;

            if (Math.Abs(sample0.angularSpeed) < 0.001 || Math.Abs(sample1.angularSpeed) < 0.001 || Math.Abs(sample2.angularSpeed) < 0.001 || Math.Abs(dw0) < 0.001 || Math.Abs(dw1) < 0.001) return;
            if (sample0.speed < 0.001 || sample1.speed < 0.001) return;

            double r0 = sample0.position.piecePosition.GetBendRadius(track);
            double d0 = sample0.trackPiece.angle < 0 ? -1.0 : 1.0;

            double r1 = sample1.position.piecePosition.GetBendRadius(track);
            double d1 = sample1.trackPiece.angle < 0 ? -1.0 : 1.0;

            if (sample0.position.angle * d0 < 0 || sample1.position.angle * d1 < 0) return;
            if (sample0.angularSpeed * d0 < 0 || sample1.angularSpeed * d1 < 0) return;
            if (dw0 * d0 <= 0 && dw1 * d1 <= 0) return;

            // Solve next system of linear equations (Ax=b):
            // 5*x + 2*y - 4*z = -7
            // 3*x - 7*y + 6*z = 38
            // 4*x + 1*y + 5*z = 43

            // Create matrix "A" with coefficients 
            var matrixA = DenseMatrix.OfArray(new[,] { 
                { d0 * sample0.speed * sample0.speed / Math.Sqrt(r0), -physicsParams.slipDownforceFactor * d0 * sample0.speed - sample0.position.angle * sample0.speed }, 
                { d1 * sample1.speed * sample1.speed / Math.Sqrt(r1), -physicsParams.slipDownforceFactor * d1 * sample1.speed - sample1.position.angle * sample1.speed }, 
            });
            //Console.WriteLine(@"Matrix 'A' with coefficients");
            //Console.WriteLine(matrixA);
            //Console.WriteLine();

            // Create vector "b" with the constant terms.
            var vectorB = new DenseVector(new[] { 
                dw0 + physicsParams.slipDampeningFactor * sample0.angularSpeed, 
                dw1 + physicsParams.slipDampeningFactor * sample1.angularSpeed, 
            });
            //Console.WriteLine(@"Vector 'b' with the constant terms");
            //Console.WriteLine(vectorB);
            //Console.WriteLine();

            // 1. Solve linear equations using LU decomposition
			MathNet.Numerics.Control.DisableParallelization = true;
            var resultX = matrixA.LU().Solve(vectorB);
            //Console.WriteLine(@"1. Solution using LU decomposition");
            //Console.WriteLine(resultX);
            //Console.WriteLine();

            double centrifugal = resultX[0];
            double angleAcceleration = resultX[1];


            if (centrifugal > 0.01 && angleAcceleration > 0.00001) {

                if (centrifugal < 0.3) {
                    logger.Log("Bug!");
                }

                centrifugalForceFactorSamples.AddSample(centrifugal);
                angleAccelerationFactorSamples.AddSample(angleAcceleration);

                physicsParams.slipFactorsKnown = true;
                physicsParams.slipCentrifugalForceFactor = centrifugalForceFactorSamples.GetMedian();
                physicsParams.slipAngleAccelerationFactor = angleAccelerationFactorSamples.GetMedian();
            }


           /* double z = -w0 * physicsParams.slipDampeningFactor - angle1 * sample1.speed * physicsParams.slipSpringFactor;
            currentCentrifugalForce = w1 - w0 - z;

            double bendRadius = sample1.position.piecePosition.GetBendRadius(track);
            double bendDirection = piece.angle < 0 ? -1.0 : 1.0;

			PrintTabulated(new double[] {
				bendRadius,
				bendDirection,
				sample1.speed, 
				angle1, 
				w0, 
				w1 - w0, 
			});

            if (Math.Abs(currentCentrifugalForce) > 0.01) {

                double f = sample1.speed * sample1.speed / Math.Sqrt(bendRadius);
                double newCentrifugalForceFactor = (currentCentrifugalForce * bendDirection + 0.3 * sample1.speed) / f;

                //logger.Log(string.Format("{0:0.000}, {1:0.000}, {2:0.000}, {3:0.000}, {4:0.000}, {5:0.000} {6}",
                //    force, bendDirection, w0, w1, angle1, sample1.speed, piece.pieceIndex));

                //logger.Log(string.Format("Force: {0}, Force factor: {1}", force, newCentrifugalForceFactor));

                double r = bendDirection * physicsParams.slipCentrifugalForceFactor * sample1.speed * sample1.speed / (currentCentrifugalForce + bendDirection * physicsParams.slipDownforceFactor * sample1.speed);
                r = r * r;

                //if (track.pieces[sample1.position.piecePosition.pieceIndex].isSwitch)
                //logger.Log(string.Format("Piece index: {0}, {1:0.0} -> {2:0.0}, Speed: {3:0.000}, F: {4:0.000}, Slip: {5:0.000}, R: {6:0.000}",
                //    sample1.position.piecePosition.pieceIndex, sample1.position.piecePosition.GetStartLaneBendRadius(track), sample1.position.piecePosition.GetEndLaneBendRadius(track),
                //    sample1.speed, currentCentrifugalForce, newCentrifugalForceFactor, r));

                if (sample1.position.piecePosition.lane.startLaneIndex == sample1.position.piecePosition.lane.endLaneIndex) {
                    centrifugalForceFactorSamples.AddSample(newCentrifugalForceFactor);

                    physicsParams.slipCentrifugalForceFactorKnown = true;
                    physicsParams.slipCentrifugalForceFactor = newCentrifugalForceFactor;// centrifugalForceFactorSamples.GetMedian();
                }
            }
            else {
                //logger.Log("No slip");
            }
            //else if (physicsParams.minSlipForceKnown && minSlipForceSamples.samples.Count > 0) {
                //physicsParams.minSlipForce = minSlipForceSamples.GetMedian();
            //}*/

		}



		/*void foo() {
            if (samples.Count < 3) return;

            Sample sample0 = GetSample(2);
            Sample sample1 = GetSample(1);
            Sample sample2 = GetSample(0);

            double angle0 = sample0.position.angle;
            double angle1 = sample1.position.angle;
            double angle2 = sample2.position.angle;

            double delta0 = angle1 - angle0;
            double delta1 = angle2 - angle1;

            Track.Piece piece = track.pieces[sample1.position.piecePosition.pieceIndex];

            //double trackDeltaAngle = piece.angle / piece.GetLaneLength(sample1.position.piecePosition.lane.startLaneIndex) * sample2.speed;

            //logger.Log(sample1.speed + " " + sample1.position.piecePosition.inPieceDistance);
            //logger.Log(string.Format("Angle: {0:0.00000}, d: {1:0.000}, dd: {2:0.000}, est: {3:0.000}", angle1, delta1, delta1 - delta0, delta0 + 0.223 - delta0 * 0.1051 - angle1 * 0.00807));
            //logger.Log(string.Format("Angle: {0:0.00000}, d: {1:0.000}, dd: {2:0.000}, est: {3:0.000}", angle1, delta1, delta1 - delta0, delta0 + 0.11692 - delta0 * 0.1051 - angle1 * 0.0077));

			double force = piece.isBend ? sample1.speed * sample1.speed / sample1.position.piecePosition.GetBendRadius(track) : 0.0;
			double bendDirection = piece.angle < 0 ? -1.0 : 1.0;

			//logger.Log(string.Format("{0:0.0000} {1:0.0000} {2:0.0000}", track.lanes[sample1.position.piecePosition.lane.startLaneIndex].distanceFromCenter, 
			//                         piece.radius, sample1.position.piecePosition.GetBendRadius(track)));

			double minForce = force + 1 / (bendDirection * physicsParams.slipForceFactor) * (delta0 - delta1 - delta0 * physicsParams.slipDampeningFactor - angle1 * sample1.speed * physicsParams.slipSpringFactor);
			//logger.Log(string.Format("{0:0.0000} {1:0.0000}", force, minForce));

			double approxDelta1 = delta0 + Math.Max(0.0, (force - minForce)) * bendDirection * physicsParams.slipForceFactor * 0.0 - delta0 * physicsParams.slipDampeningFactor - angle1 * sample1.speed * physicsParams.slipSpringFactor;

            //logger.Log(sample1.speed);
			//logger.Log(string.Format("Angle: {0:0.00000}, d: {1:0.000}, dd: {2:0.000}, est: {3:0.000} ({4:0.000})", angle1, delta1, delta1 - delta0, approxDelta1, approxDelta1 - delta1));

			if (piece.isBend) {
				logger.Log(string.Format("speed {0:0.00000}, radius {1:0.000}, force: {2:0.000}", 
				           sample1.speed, sample1.position.piecePosition.GetBendRadius(track), delta1 - approxDelta1));
			}
        }*/

		public Sample GetSample(int i) {
			return i < samples.Count ? samples[samples.Count - 1 - i] : null;
		}

		void WriteData(string tag, double[] data) {
			for (int i=0; i<5; ++i) {
				try {
					using (System.IO.StreamWriter file = new System.IO.StreamWriter(tag + ".txt", true)) {
						file.WriteLine(TabulatedString(data));
					}

					return;
				}
				catch (System.IO.IOException) {
				}
			}
		}
	}
}

