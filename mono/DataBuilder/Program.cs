﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using Bot;

namespace DataBuilder {
    class Program {

        System.IO.StreamWriter file;

        int indentationLevel = 0;


        static void Main(string[] args) {
            Program program = new Program();
            program.Run(args);

        }

        void Run(string[] args) {
            using (file = new System.IO.StreamWriter("Data.cs", false)) {

                WriteLine("using System;");
                WriteLine("using System.Collections.Generic;");
                WriteLine("namespace Bot {{");

                indentationLevel++;
                WriteClass();
                indentationLevel--;

                WriteLine("}}");
            }
        }

        void WriteClass() {
            WriteLine("class Data {{");


            indentationLevel++;
            WriteFillMethod();
            indentationLevel--;

            WriteLine("}}");
        }

        void WriteFillMethod() {
            WriteLine("public static void FillData(DataHandler handler) {{");

            string[] files = Directory.GetFiles("data", "*.txt.txt");

            indentationLevel++;
            foreach (string file in files) {
                WriteLine("{{");
                indentationLevel++;

                string name = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(file));

                string[] components = name.Split('_');
                if (components.Length < 3) continue;

                if (components[0] == "s") {
                    int length = int.Parse(components[1]);
                    int width = int.Parse(components[2]);

                    double[][] values = LoadData(file, 2);
                    WriteStraightSwitchData(width, length, values);
                }
                else if (components[0] == "b" && components[components.Length - 1] == "l") {
                    int angle = int.Parse(components[1]);
                    int r0 = int.Parse(components[2]);
                    int r1 = int.Parse(components[3]);

                    double[][] lengthValues = LoadData(file, 2);
                    double[][] radiiValues = LoadData(string.Format("data/b_{0}_{1}_{2}_r.txt.txt", angle, r0, r1), 2);

                    WriteBendSwitchData(angle, r0, r1, lengthValues, radiiValues);
                }

                indentationLevel--;
                WriteLine("}}");
            }
            indentationLevel--;

            WriteLine("}}");
        }

        void WriteStraightSwitchData(int width, int length, double[][] lengthValues) {
            WriteLine("StraightSwitchData data = new StraightSwitchData() {{ length = {0}, width = {1},", length, width);

            {
                Array.Sort(lengthValues, (double[] a, double[] b) => {
                    return a[1].CompareTo(b[1]);
                });

                WriteLine("    rawLengths = new List<double>(new double[] {{");
                string line = "";
                Pick<double[]>(lengthValues, 10, (int i, double[] value) => {
                    line += value[1].ToString(CultureInfo.InvariantCulture) + ", ";
                });
                WriteLine("        " + line);
                WriteLine("    }}),");
            }

            WriteLine("}};");
            WriteLine("data.ProcessLengthData();");
            WriteLine("handler.AddData(data);");
        }

        void WriteBendSwitchData(int angle, int r0, int r1, double[][] lengthValues, double[][] radiiValues) {
            WriteLine("BendSwitchData data = new BendSwitchData() {{ angle = {0}, r0 = {1}, r1 = {2},", angle, r0, r1);

            {
                Array.Sort(lengthValues, (double[] a, double[] b) => {
                    return a[1].CompareTo(b[1]);
                });

                WriteLine("    rawLengths = new List<double>(new double[] {{");
                string line = "";
				Pick<double[]>(lengthValues, 10, (int i, double[] value) => {
                    line += value[1].ToString(CultureInfo.InvariantCulture) + ", ";
                });
                WriteLine("        " + line);
                WriteLine("    }}),");
            }

            if (radiiValues != null) {

                BendSwitchData data = new BendSwitchData() {
                    angle = angle, r0 = r0, r1 = r1
                };

                for (int i = 0; i < radiiValues.Length; ++i) {
                    data.rawRadiis.Add(new KeyValuePair<double, double>(radiiValues[i][0], radiiValues[i][1]));
                }

                data.ProcessRadiiData();


                Array.Sort(radiiValues, (double[] a, double[] b) => {
                    return a[0].CompareTo(b[0]);
                });

                WriteLine("    rawRadiis = new List<KeyValuePair<double, double>>(new KeyValuePair<double, double>[] {{");
				Pick<double[]>(radiiValues, 15, (int i, double[] value) => {
                    //line += value[0].ToString(CultureInfo.InvariantCulture) + ", ";
                    WriteLine("        new KeyValuePair<double, double>(" + value[0].ToString(CultureInfo.InvariantCulture) + ", " + value[1].ToString(CultureInfo.InvariantCulture) + " ),");
                });

                WriteLine("    }}),");
                WriteLine("    polynomialConstant = {0},", data.polynomialConstant.ToString(CultureInfo.InvariantCulture));
                WriteLine("    polynomialLinear = {0},", data.polynomialLinear.ToString(CultureInfo.InvariantCulture));
                WriteLine("    polynomialQuadratic = {0},", data.polynomialQuadratic.ToString(CultureInfo.InvariantCulture));
                WriteLine("    switchRadiisKnown = {0},", data.switchRadiisKnown ? "true" : "false");
                WriteLine("    processedRawRadiisCount = {0},", 15);
            }

            WriteLine("}};");
            WriteLine("data.ProcessLengthData();");
            WriteLine("handler.AddData(data);");
        }

		delegate void PickHandler<T>(int index, T value);

        void Pick<T>(T[] values, int count, PickHandler<T> handler) {
            if (values.Length <= count) {
                for (int i = 0; i < values.Length; ++i) {
                    handler(i, values[i]);
                }
            }
            else {
                for (int i = 0; i < count; ++i) {
                    double t = i / (double)count;
                    int j = Math.Max(0, Math.Min(values.Length - 1, (int)(values.Length * t)));
                    handler(j, values[j]);
                }
            }
        }

        double[][] LoadData(string file, int columnCount) {

            List<double[]> lines = new List<double[]>();

            using (StreamReader reader = new StreamReader(file)) {
                string line;

                while ((line = reader.ReadLine()) != null) {
                    string[] components = line.Split('\t');
                    if (components.Length != columnCount) continue;

                    double[] values = new double[columnCount];

                    bool failed = false;
                    for (int i = 0; i < components.Length; ++i) {
                        double value;
                        if (double.TryParse(components[i], out value)) {
                            values[i] = value;
                        }
                        else {
                            failed = true;
                            break;
                        }
                    }

                    if (!failed) {
                        lines.Add(values);
                    }
                }
            }

            return lines.ToArray();
        }

        public void WriteLine(string format, params object[] args) {
            for (int i = 0; i < indentationLevel; ++i) file.Write("    ");
            file.WriteLine(format, args);
        }
    }
}
